// Supported with union (c) 2018-2021 Union team
// Add your sources this

// Automatically generated block
#pragma region Includes
#include "SafeQueue.cpp"
#include "CustomTypes.cpp"
#include "Chat.cpp"
#include "External.cpp"
#include "Global.cpp"
#include "Helpers.cpp"
#include "Utils.cpp"
#include "Debugger.cpp"
#include "PlayerAnimation.cpp"
#include "LocalNpc.cpp"
#include "RemoteNpc.cpp"
#include "SyncHelpers.cpp"
#include "Client.cpp"
#include "Server.cpp"
#include "MappedPort.cpp"
#include "GameStats.cpp"
#include "PacketProcessor.cpp"
#include "DamageProcessor.cpp"
#include "VisibleNpcsUpdater.cpp"
#include "SpellCastProcessor.cpp"
#include "ReviveFriendLoop.cpp"
#include "Hooks.cpp"
#include "Utils_After.cpp"
#include "Plugin.cpp"
#include "UnionHooks.cpp"
#pragma endregion

// ...
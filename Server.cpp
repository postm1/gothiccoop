namespace GOTHIC_ENGINE {

    int CoopServerThread()
    {
        if (enet_initialize() != 0)
        {
            ChatLog("An error occurred while initializing ENet.");
            return EXIT_FAILURE;
        }
        atexit(enet_deinitialize);

        ENetAddress address;
        ENetHost* server;
        enet_address_set_host(&address, "0.0.0.0");
        address.port = portSocket;
        server = enet_host_create(&address, 32, 2, 0, 0);
        if (server == NULL)
        {
            ChatLog("An error occurred while trying to create an ENet server host.");
            return EXIT_FAILURE;
        }

        ChatLog(string::Combine("(Server) Ready (v. %i).", COOP_VERSION), GFX_GREEN);
        while (true) {

            ENetEvent event;

            auto eventStatus = enet_host_service(server, &event, 1);
            if (eventStatus > 0) {
                ReadyToBeReceivedPackets.enqueue(event);
            }

            // get online + 1 server player
            lastOnline = server->connectedPeers + 1;
            

            
            if (!ReadyToBeDistributedPackets.isEmpty()) {
                auto jsonPacket = ReadyToBeDistributedPackets.dequeue();
                auto playerId = jsonPacket["id"].get<std::string>();


                for (size_t i = 0; i < server->peerCount; i++) {
                    auto peer = &server->peers[i];

                    if (!peer)
                    {
                        continue;
                    }

                    auto pNpc = (RemoteNpc*)peer->data;

                    // don't send info to npc's owner (client summons)
                    if (pNpc && pNpc->connectIdOwner == peer->connectID)
                    {
                        //cmd << "Ignore, the same connect ID..." << endl;
                        //continue;
                    }

                    /*
                    if (pNpc->npc)
                    {
                        cmd << "Distr: playerId: " << playerId.c_str() << " connectId: " << pNpc->connectIdOwner << " peerId: " << peer->connectID << "|" << pNpc->npc->GetInstanceName() << ";" << pNpc->name << endl;
                    }
                    else
                    {
                        cmd << "No npc distr: playerId: " << playerId.c_str() << " peerId: " << peer->connectID << endl;
                    }
                    */
                        
                    // FIXME! thread dungerous
                    if (pNpc && !pNpc->name.Compare(playerId.c_str())) {

                        //cmd << "DistrFinal: " << pNpc->name << ";" << playerId.c_str() << endl;

                        auto stringPacket = jsonPacket.dump();
                        ENetPacket* packet = enet_packet_create(stringPacket.c_str(), strlen(stringPacket.c_str()) + 1, ENET_PACKET_FLAG_RELIABLE);
                        enet_peer_send(peer, 0, packet);
                    }
                }
            }

            if (!ReadyToSendJsons.isEmpty()) {
                auto j = ReadyToSendJsons.dequeue();

                auto updateJSON = j.dump();

                //cmd << "ServerSend: " << updateJSON.c_str() << endl;

                auto type = j["type"].get<int>();

                if (IsUnreliable((UpdateType)type))
                {
                    ENetPacket* packet = enet_packet_create(updateJSON.c_str(), strlen(updateJSON.c_str()) + 1, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT);
                    enet_host_broadcast(server, 0, packet);
                }
                else
                {
                    ENetPacket* packet = enet_packet_create(updateJSON.c_str(), strlen(updateJSON.c_str()) + 1, ENET_PACKET_FLAG_RELIABLE);
                    enet_host_broadcast(server, 0, packet);
                }

                
            }
        }
    }
}
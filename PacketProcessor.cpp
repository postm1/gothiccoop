namespace GOTHIC_ENGINE {
    void ProcessCoopPacket(json e, ENetEvent packet) {
        auto id = e["id"].get<std::string>();
        auto type = e["type"].get<int>();



        //cmd << "Packet: " << id.c_str() << " " << GetPacketTypeAsString((UpdateType)type) << " "  << e.dump().c_str() << endl;

        
        auto npcToSync = SyncNpcs.count(id.c_str()) ? SyncNpcs[id.c_str()] : NULL;

        if (type == SYNC_PLAYER_NAME) {
            auto connectId = e["connectId"].get<enet_uint32>();

            if (connectId == packet.peer->connectID) {
                std::string name = e["name"].get<std::string>();
                MyselfId = name.c_str();
            }
            return;
        }

        if (type == PLAYER_DISCONNECT) {
            string name = e["name"].get<std::string>().c_str();
            ChatLog(string::Combine("%s disconnected.", name));
           
            removeSyncedNpc(name);
            return;
        }

        if (IsCoopPaused) {
            return;
        }

        if (type == SYNC_SPELL_CAST) {
            //ChatLog(e.dump().c_str());
        }

        if (npcToSync == NULL) {
           // cmd << "Npc init null" << endl;

            if (IsCoopPlayer(id)) {


                //cmd << "1 Npc init null" << endl;
                npcToSync = addSyncedNpc(id.c_str());
                npcToSync->connectIdOwner = packet.peer->connectID;
                Myself->Reinit();
            }
            else if (UniqueNameToNpcList.count(id.c_str())) {

                cmd << "2 Npc init null: " << id.c_str() << endl;

                auto pNpc = UniqueNameToNpcList[id.c_str()];

                if (pNpc && Npc_IsSummon(pNpc))
                {
                    //cmd << "Ignore local summon from server" << endl;
                    return;
                }


                npcToSync = addSyncedNpc(id.c_str());
                npcToSync->connectIdOwner = packet.peer->connectID;

                if (pNpc)
                {
                    cmd << "Sync ok: " << pNpc->GetInstanceName() << endl;
                    //pNpc->UseStandAI();
                }
            }
            // no npc found
            else
            {
                //cmd << "try to create a new npc: " << id.c_str() << " type: " << GetPacketTypeAsString((UpdateType)type) << endl;

                if (type == INIT_NPC)
                {

                   // cmd << "Init npc ok " << endl;

                    auto nickname = e["nickname"].get<std::string>();
                    auto instanceId = e["instanceId"].get<std::string>();
                    auto isSummon = e["summon"].get<int>();
                    auto index = parser->GetIndex(instanceId.c_str());

                    // add new npc only if it is a summon
                    if (isSummon)
                    {
                        //cmd << "name: " << nickname.c_str() << " inst: " << instanceId.c_str() << endl;

                        npcToSync = addSyncedNpc(id.c_str());
                        npcToSync->npcInstName = instanceId.c_str();
                        npcToSync->uniqName = id.c_str();
                        npcToSync->connectIdOwner = packet.peer->connectID;

                        //cmd << packet.peer->connectID << ";" << npcToSync->connectIdOwner << endl;
                    }
                    else
                    {
                        //cmd << "Sync not summon npc" << endl;
                    }

                }
                else
                {

                }
            }
        }

        if (npcToSync) {
            npcToSync->localUpdates.push_back(e);
        }
    }

    void ProcessServerPacket(ENetEvent packet) {
        switch (packet.type) {
        case ENET_EVENT_TYPE_CONNECT:
        {
            auto playerName = string::Combine("FRIEND_%i", GetFreePlayerId());
            ChatLog(string::Combine("(Server) We got a new connection %s", string(playerName)));

            json j;
            j["id"] = "HOST";
            j["type"] = SYNC_PLAYER_NAME;
            j["name"] = string(playerName).ToChar();
            j["connectId"] = packet.peer->connectID;
            ReadyToSendJsons.enqueue(j);
            bytesSent += j.size();

            packet.peer->data = addSyncedNpc(playerName);

            if (Myself) {
                Myself->Reinit();
            }
            for each (auto n in BroadcastNpcs)
            {
                n.second->Reinit();
            }
            break;
        }
        case ENET_EVENT_TYPE_RECEIVE:
        {
            if (!packet.packet)
            {
                cmd << "No packet in SERVER ENET_EVENT_TYPE_RECEIVE" << endl;
                return;
            }

            auto currentPlayer = (RemoteNpc*)packet.peer->data;
            const char* data = (const char*)packet.packet->data;
            SaveNetworkPacket(data);

           

            auto j = json::parse(data);

            //cmd << "Server recieve: " << j.dump().c_str() << " " << currentPlayer->name << endl;

            //j["id"] = currentPlayer->name.ToChar();

            auto id = j["id"].get<std::string>();
            auto type = j["type"].get<int>();


            
            // if packet is from client and contains player, change id, only in this case
            if (id == "_player_")
            {
                auto remoteNpc = (RemoteNpc*)(packet.peer->data);

                if (remoteNpc)
                {
                    id = remoteNpc->name.ToChar();
                    j["id"] = id;
                }
               

                //cmd << "id change: " << id.c_str() << endl;
            }
           

            /*
            if (currentPlayer && currentPlayer->connectIdOwner == 0)
            {
                //cmd << "Set connect id for client: " << packet.peer->connectID << endl;

                currentPlayer->connectIdOwner = packet.peer->connectID;
            }
            */


            ReadyToBeDistributedPackets.enqueue(j);
            bytesSent += j.size();
            ProcessCoopPacket(j, packet);
            break;
        }
        case ENET_EVENT_TYPE_DISCONNECT:
        {
            auto remoteNpc = (RemoteNpc*)(packet.peer->data);
            ChatLog(string::Combine("%s disconnected.", remoteNpc->name));

            json j;
            j["id"] = "HOST";
            j["type"] = PLAYER_DISCONNECT;
            j["name"] = string(remoteNpc->name).ToChar();
            ReadyToSendJsons.enqueue(j);

            bytesSent += j.size();

            removeSyncedNpc(remoteNpc->name);
            packet.peer->data = NULL;
            break;
        }
        }
    }

    void ProcessClientPacket(ENetEvent packet) {
        switch (packet.type) {
        case ENET_EVENT_TYPE_RECEIVE:
        {

            if (!packet.packet)
            {
                cmd << "No packet in CLIENT ENET_EVENT_TYPE_RECEIVE" << endl;
                return;
            }

            const char* data = (const char*)packet.packet->data;
            SaveNetworkPacket(data);

            auto j = json::parse(data);

           // cmd << "Client recieve: " << j.dump().c_str() << endl;

            ProcessCoopPacket(j, packet);


            enet_packet_destroy(packet.packet);
            break;
        }
        case ENET_EVENT_TYPE_DISCONNECT:
        {
            ChatLog("Connection to the server lost.");
           
            wasClientDisconnect = true;

            if (ClientThread)
            {
                ClientThread->Break();
                ClientThread = NULL;
            }

            break;
        }
        }
    }

    void PacketProcessorLoop() {
        PluginState = "PacketProcessorLoop";
        if (ReadyToBeReceivedPackets.isEmpty()) {
            return;
        }

        while (!ReadyToBeReceivedPackets.isEmpty()) {
            auto packet = ReadyToBeReceivedPackets.dequeue();

            if (packet.packet)
            {
                bytesRecieved += packet.packet->dataLength;
            }
           
            if (ServerThread) {
                ProcessServerPacket(packet);
            }
            else if (ClientThread)
            {
                ProcessClientPacket(packet);
            }
            
            
        }
    }
}
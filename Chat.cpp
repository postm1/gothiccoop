﻿namespace GOTHIC_ENGINE {
    struct ChatLine
    {
        string text;
        zCOLOR color;
        zVEC2 pos;
    };

    class Chat
    {
    private:
        bool isShowing = true;

#ifdef NEWBALANCE_MOD
        unsigned int chatLines = 32;
#else
        unsigned int chatLines = 32;
#endif // NEWBALANCE_MOD
        std::vector<ChatLine> lines;
        SafeQueue<ChatLine> readyToBeDisplatedLines;
        zCView* pView = NULL;

        std::vector<ChatLine> constlines;

    public:
        void Render() {
            while (!readyToBeDisplatedLines.isEmpty()) {
                if (lines.size() < chatLines)
                    lines.push_back(readyToBeDisplatedLines.dequeue());
                else
                {
                    lines.erase(lines.begin());
                    lines.push_back(readyToBeDisplatedLines.dequeue());
                }
            }

            if (this->IsShowing() == true)
            {

                if (!pView)
                {
                    pView = new zCView(0, 0, 8196, 8196);
                    pView->SetFont(zSTRING("Font_Old_10_White_Hi.TGA"));
                    pView->SetFontColor(GFX_WHITE);
                    screen->InsertItem(pView);
                }
               
                int yPos = 50;
                int xPos = 50;

#ifdef NEWBALANCE_MOD
                xPos = 120;
                yPos = 380;
#endif // NEWBALANCE_MOD

                pView->ClrPrintwin();

                for (unsigned int i = 0; i < lines.size(); i++)
                {
                    if (lines[i].text.Length() > 0)
                    {
                        pView->SetFontColor(lines[i].color);
                        pView->Print(xPos, yPos, zSTRING(lines[i].text));
                    }
                    yPos += 150;
                }

                
                for (unsigned int i = 0; i < constlines.size(); i++)
                {
                    if (constlines[i].text.Length() > 0)
                    {
                        pView->SetFontColor(constlines[i].color);
                        pView->Print(constlines[i].pos.n[0], constlines[i].pos.n[1], zSTRING(constlines[i].text));
                    }
                }
            }
        };

        bool IsShowing() {
            return isShowing;
        }

        void ToggleShowing() {
            isShowing = !isShowing;
        }

        unsigned int GetLines() {
            return lines.size();
        }

        void AddLine(string text, zCOLOR color = zCOLOR(255, 255, 255, 255)) {
            ChatLine line;
            line.text = text;
            line.color = color;

            readyToBeDisplatedLines.enqueue(line);
        }

        void AddLineConst(string text, zCOLOR color = zCOLOR(255, 255, 255, 255), int x = 0, int y = 0) {
            ChatLine line;
            line.text = text;
            line.color = color;
            line.pos = zVEC2(x, y);

            constlines.push_back(line);
        }

        void Clear() {
            lines.clear();
        }

        void ClearConst() {
            constlines.clear();
        }
    };
}

namespace GOTHIC_ENGINE {
    enum UpdateType
    {
        SYNC_POS,
        SYNC_HEADING,
        SYNC_ANIMATION,
        SYNC_WEAPON_MODE,
        INIT_NPC,
        DESTROY_NPC,
        SYNC_ATTACKS,
        SYNC_ARMOR,
        SYNC_WEAPONS,
        SYNC_HP,
        SYNC_TIME,
        SYNC_HAND,
        SYNC_MAGIC_SETUP,
        SYNC_SPELL_CAST,
        SYNC_REVIVED,
        SYNC_PROTECTIONS,
        SYNC_PLAYER_NAME,
        PLAYER_DISCONNECT,
        SYNC_TALENTS,
        SYNC_BODYSTATE,
        SYNC_OVERLAYS,
        SYNC_HELMET,
        SYNC_SHIELD_SLOT, // left dual, shield
        SYNC_TORCH_SLOT, // torch
        SYNC_LEFT_HAND_SLOT, // use items like potions
        SYNC_RIGHT_HAND_SLOT, // use items in mobinters/interactions,
        SYNC_TROPHY_SLOT, // slot like dragon skull
        SYNC_DROPITEM, //drop item on the ground
        SYNC_TAKEITEM, //take item from the ground
        SYNC_THIGH, //lighter on hero leg, NB only
        SYNC_ANIM_NB,
        SYNC_CHAGEGUILD, //transform
    };

    struct PlayerHit
    {
        string npcUniqueName;
        float damage;
        oCNpc* npc;
        oCNpc* attacker;
        int isUnconscious;
        bool isDead;
        unsigned long damageMode;
    };

    struct SpellCast
    {
        oCNpc* npc;
        oCNpc* targetNpc;
        string npcUniqueName;
        string targetNpcUniqueName;
    };
}
namespace GOTHIC_ENGINE {
    class RemoteNpc
    {
    public:
        string name;
        string playerNickname = "";
        oCNpc* npc = NULL;
        bool destroyed = false;
        bool isSpawned = false;
        bool hasNpc = false;
        bool hasModel = false;
        std::vector<json> localUpdates;

        zVEC3 lastPositionFromServer;
        float lastHeadingFromServer = -1;
        int lastHpFromServer = -1;
        int lastWeaponMode = -1;
        int lastWeapon1 = -1;
        int lastWeapon2 = -1;
        int lastArmor = -1;
        int lastHelmet = -1;
        int lastShield = -1;
        int lastTorch = -1;
        int lastLeftHand = -1;
        int lastRightHand = -1;
        int lastTrophy = -1;
        int lastThigh = -1;

        int NB_AniMode = -1;
        int NB_AniId = -1;

        int NB_LastAniId = -1;
        enet_uint32 connectIdOwner = 0;

        Timer npcTimer;

        zSTRING npcInstName;
        zSTRING uniqName;
        zSTRING playerNickName;

        zSTRING lastSpellInstanceName;
        oCItem* spellItem;
        std::map<oCVob*, bool> syncedNpcItems;

        RemoteNpc(string playerName) {
            name = playerName;
        }

        void Reset()
        {
            lastHpFromServer = -1;
            lastWeaponMode = -1;
            lastWeapon1 = -1;
            lastWeapon2 = -1;
            lastArmor = -1;
            lastHelmet = -1;
            lastShield = -1;
            lastTorch = -1;
            lastLeftHand = -1;
            lastRightHand = -1;
            lastTrophy = -1;
            lastThigh = -1;
            lastSpellInstanceName = "";
            lastHeadingFromServer = -1;
            lastPositionFromServer = zVEC3(0, 0, 0);
        }

        void UpdateAnims()
        {
            if (npcTimer[0u].Await(1000))
            {
                if (IsCoopPlayer(npc->GetObjectName()))
                {
                    npc->anictrl->StopTurnAnis();
                }
                
            }

            if (NB_AniId != -1) {

                npc->anictrl->StopTurnAnis();

                //NB_PrintWin(Z("Start ani #1: ") + Z NB_AniId);

                npc->GetModel()->StartAni(NB_AniId, NB_AniMode);
                NB_LastAniId = NB_AniId;
                NB_AniId = -1;

            };

            
            if (NB_LastAniId != -1) {
                if (npc->GetModel()->IsAniActiveEngine(NB_LastAniId)) {
                    NB_LastAniId = -1;
                }
                else {
                    //NB_PrintWin("Start ani #2: " + Z NB_LastAniId);
                    npc->GetModel()->StartAni(NB_LastAniId, NB_AniMode);
                    NB_LastAniId = -1;
                }
            }
            
        }

        void Update() {
            if (destroyed) {
                return;
            }

            if (npc == NULL && npcInstName.Length() > 0)
            {

            }
            npcTimer.ClearUnused();

            UpdateHasNpcAndHasModel();
            //RemoveCoopItemsFromGround();
            RespawnOrDestroyBasedOnDistance();

            if (hasModel)
            {
               // UpdateAnims();
            }
 

            if (npc == NULL && UniqueNameToNpcList.count(name) > 0) {


                //cmd << "Get Name form list for: " << name << endl;
                npc = UniqueNameToNpcList[name];
            }

            if (npc && IsPlayerTalkingWithNpc(npc)) {
                return;
            }

            // do not update on cutscenes
            if (!IsCoopPlayer(name) && zCCSCamera::playing) {
                return;
            }

            if (IsNpcInTot()) {
                return;
            }

            if (npc && playerNickName == "")
            {
                playerNickName = npc->name;
            }

            for (unsigned int i = 0; i < localUpdates.size(); i++)
            {
                auto update = localUpdates.front();
                localUpdates.erase(localUpdates.begin());

                auto type = update["type"].get<int>();
                PluginState = "Remote: " + name + " TYPE: " + GetPacketTypeAsString((UpdateType)type).ToChar() + " " + type;


                if (IsUniqPacket((UpdateType)type) || true)
                {
                    //cmd << PluginState.c_str() << endl;
                }
                

                switch (type) {
                case INIT_NPC:
                {
                    UpdateInitialization(update);
                    break;
                }
                case SYNC_POS:
                {
                    UpdatePosition(update);
                    break;
                }
                case SYNC_HEADING:
                {
                    UpdateAngle(update);
                    break;
                }
                case SYNC_ANIMATION:
                {
                    UpdateAnimation(update);
                    break;
                }
                case SYNC_ANIM_NB:
                {
                    UpdateAnimation_NB(update);
                    break;
                }
                case SYNC_WEAPON_MODE:
                {
                    UpdateWeaponMode(update);
                    break;
                }
                case SYNC_HP:
                {
                    UpdateHp(update);
                    break;
                }
                case SYNC_TALENTS:
                {
                    UpdateTalents(update);
                    break;
                }
                case SYNC_PROTECTIONS:
                {
                    UpdateProtection(update);
                    break;
                }
                case SYNC_ARMOR:
                {
                    UpdateArmor(update);
                    break;
                }
                case SYNC_HELMET:
                {
                    UpdateHelmet(update);
                    break;
                }
                case SYNC_SHIELD_SLOT:
                {
                    UpdateShieldSlot(update);
                    break;
                }
                case SYNC_TORCH_SLOT:
                {
                    UpdateTorchSlot(update);
                    break;
                }
                case SYNC_THIGH:
                {
                    UpdateThighSlot(update);
                    break;
                }
                case SYNC_TROPHY_SLOT:
                {
                    UpdateTrophySlot(update);
                    break;
                }
                case SYNC_LEFT_HAND_SLOT:
                {
                    UpdateLeftHandSlot(update);
                    break;
                }
                case SYNC_RIGHT_HAND_SLOT:
                {
                    UpdateRightHandSlot(update);
                    break;
                }
                case SYNC_MAGIC_SETUP:
                {
                    UpdateMagicSetup(update);
                    break;
                }
                case SYNC_HAND: {
                    UpdateHand(update);
                    break;
                }
                case SYNC_WEAPONS:
                {
                    UpdateWeapons(update);
                    break;
                }
                case SYNC_DROPITEM:
                {
                    UpdateDropItem(update);
                    break;
                }
                case SYNC_TAKEITEM:
                {
                    UpdateTakeItem(update);
                    break;
                }
                case SYNC_SPELL_CAST:
                {
                    UpdateSpellCasts(update);
                    break;
                }
                case SYNC_ATTACKS:
                {
                    UpdateAttacks(update);
                    break;
                }
                case SYNC_TIME: {
                    UpdateTime(update);
                    break;
                }
                case SYNC_REVIVED: {
                    UpdateRevived(update);
                    break;
                }
                case DESTROY_NPC:
                {
                    DestroyNpc();
                    break;
                }
                case SYNC_BODYSTATE:
                {
                    UpdateBodystate(update);
                    break;
                }
                case SYNC_OVERLAYS:
                {
                    UpdateOverlays(update);
                    break;
                }
                case SYNC_CHAGEGUILD:
                {
                    UpdateVisual(update);
                    break;
                }
                }
            }

            PluginState = "UpdateSyncNpcs";
            UpdateNpcBasedOnLastDataFromServer();
        }

        void UpdateInitialization(json update) {

            //cmd << "UpdateInitialization: " << update.dump().c_str() << endl;

            if (npc == NULL) {
                auto x = update["x"].get<float>();
                auto y = update["y"].get<float>();
                auto z = update["z"].get<float>();
                auto nickname = update["nickname"].get<std::string>();

                // check xyz for valid values
                if (CheckVarsForNAN(x, y, z))
                {
                    lastPositionFromServer = player->GetPositionWorld();
                }
                else
                {
                    lastPositionFromServer = zVEC3(x, y, z);
                }

                playerNickname = nickname.c_str();

                


                

                if (IsCoopPlayer(name)) {
                    InitCoopFriendNpc();
                    UpdateHasNpcAndHasModel();
                }
                else if (!npc && npcInstName.Length() > 0) {


                    // hardcode checking for some summons, because they have checking for player's variables
                    if (npcInstName == "PET_JINA")
                    {
                        npcInstName = "PET_JINA_MULTIPLAYER_REMOTE";
                    }
                   
                    int index = parser->GetIndex(npcInstName);

                    if (index != -1)
                    {
                        cmd << "===========\n!!!! SummonNpc: " << npcInstName << " uniq: " << uniqName << endl;


                        NB_BlockNextSummon();

                        SaveParserVars();
                        oCNpc* newNpc = ogame->GetSpawnManager()->SummonNpc(index, lastPositionFromServer, 0.0f);

                        RestoreParserVars();

 



                        if (Npc_IsSummon(newNpc))
                        {
                            //turn off all AI for remote summon
                            newNpc->state.EndCurrentState();
                            newNpc->state.ClearAIState();
                            newNpc->startAIState = 0;
                            newNpc->ClearPerceptionLists();
                            newNpc->UseStandAI();
                            remoteSummons.InsertEnd(newNpc);
                        }

                        if (newNpc && newNpc->GetModel())
                        {
                            //newNpc->GetModel()->StartAni("T_SPAWN", 0);
                        }


                        npc = newNpc;


                        //cmd << "NewNpc: " << npc->GetInstanceName() << " " << npc->GetObjectName() << endl;

                        UpdateHasNpcAndHasModel();

                        if (NpcToUniqueNameList.count(npc) == 0)
                        {
                            NpcToUniqueNameList[npc] = uniqName.ToChar();
                            //cmd << "NpcToUniqueNameList added: " << uniqName << endl;
                        }

                        if (UniqueNameToNpcList.count(uniqName.ToChar()) == 0)
                        {
                            UniqueNameToNpcList[uniqName.ToChar()] = npc;

                            //cmd << "UniqueNameToNpcList added: " << uniqName << endl;
                        }

                        
                        //cmd << "==========" << endl;
                    }

                    //ogame->spawnman->InsertNpc(npc, *lastPositionFromServer);
                }
            }
        }

        void UpdatePosition(json update) {
            auto x = update["x"].get<float>();
            auto y = update["y"].get<float>();
            auto z = update["z"].get<float>();

            if (CurrentWorldTOTPosition) {
                auto newPosition = zVEC3(x, y, z);
                auto totPos = *CurrentWorldTOTPosition;
                int dist = newPosition.Distance(totPos);

                if (dist < 500) {
                    destroyed = true;
                    return;
                }
            }

            if (CheckVarsForNAN(x, y, z))
            {
                lastPositionFromServer = player->GetPositionWorld();
            }
            else
            {
                lastPositionFromServer = zVEC3(x, y, z);
            }
        }

        void UpdateAngle(json update) {
            auto h = update["h"].get<float>();
            lastHeadingFromServer = h;
            if (hasModel) {
                npc->ResetRotationsWorld();
                npc->RotateWorldY(h);
            }
        }


        void UpdateAnimation(json update) {
            if (hasModel) {

                
               auto a = update["a"].get<int>();
               npc->GetModel()->StartAni(a, COOP_MAGIC_NUMBER);
            }
        }

        void UpdateAnimation_NB(json update) {
            if (hasModel) {


                auto aniId = update["aniId"].get<int>();
                auto aniMode = update["aniMode"].get<int>();


                NB_AniId = aniId;
                NB_AniMode = aniMode;

              
            }
        }

        void UpdateWeaponMode(json update) {
            if (hasModel) {

                if (npc->guild == GIL_MEATBUG)
                {
                    return;
                }

                auto wm = update["wm"].get<int>();
                lastWeaponMode = wm;
                npc->SetWeaponMode2(wm);

                if (!IsCoopPlayer(name)) {
                    npc->GetEM()->KillMessages();
                    npc->ClearEM();
                    npc->state.ClearAIState();
                }
            }
        }

        void UpdateHp(json update) {
            auto hp = update["hp"].get<int>();
            auto hpMax = update["hp_max"].get<int>();

            if (!IsCoopPlayer(name) && hp == 0) {
                if (hasNpc) {

                    cmd << "Npc has 0 health: " << name << " " << npc->GetInstanceName() << endl;

                    if (!npc->IsDead() && Npc_IsSummon(npc))
                    {
                        npc->SetAttribute(NPC_ATR_HITPOINTS, 0);
                        npc->DoDie(NULL);
                    }
                    else if (!npc->IsDead() && KilledByPlayerNpcNames.count(name) == 0) {
                        npc->SetAttribute(NPC_ATR_HITPOINTS, 1);
                    } else {
                        npc->SetAttribute(NPC_ATR_HITPOINTS, 0);
                        NB_CallOnDead(player, npc);
                        npc->DoDie(player);
                    }
                }
                lastHpFromServer = -1;
                return;
            }

            lastHpFromServer = hp;
            if (hasNpc) {
                npc->SetAttribute(NPC_ATR_HITPOINTS, hp);
                npc->SetAttribute(NPC_ATR_HITPOINTSMAX, hpMax);
            }
        }

        void UpdateTalents(json update) {
            auto t0 = update["t0"].get<int>();
            auto t1 = update["t1"].get<int>();
            auto t2 = update["t2"].get<int>();
            auto t3 = update["t3"].get<int>();

            if (hasNpc) {
                npc->SetTalentSkill(oCNpcTalent::NPC_TAL_1H, t0);
                npc->SetTalentSkill(oCNpcTalent::NPC_TAL_2H, t1);
                npc->SetTalentSkill(oCNpcTalent::NPC_TAL_BOW, t2);
                npc->SetTalentSkill(oCNpcTalent::NPC_TAL_CROSSBOW, t3);
            }
        }

        void UpdateProtection(json update) {
            auto p0 = update["p0"].get<int>();
            auto p1 = update["p1"].get<int>();
            auto p2 = update["p2"].get<int>();
            auto p3 = update["p3"].get<int>();
            auto p4 = update["p4"].get<int>();
            auto p5 = update["p5"].get<int>();
            auto p6 = update["p6"].get<int>();
            auto p7 = update["p7"].get<int>();

            if (hasNpc) {
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(0), p0);
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(1), p1);
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(2), p2);
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(3), p3);
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(4), p4);
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(5), p5);
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(6), p6);
                npc->SetProtectionByIndex(static_cast<oEIndexDamage>(7), p7);
            }
        }

        void UpdateArmor(json update) {
            if (hasNpc) {
                auto armor = update["armor"].get<std::string>();


                //cmd << "UpdateArmor: " << update.dump().c_str() << endl;


                auto currentArmor = npc->GetEquippedArmor();
                if (currentArmor) {

                    //cmd << "current armor remove" << endl;

                    lastArmor = -1;
                    npc->UnequipItem(currentArmor);
                }

                if (armor != "NULL") {
                    int insIndex = parser->GetIndex(armor.c_str());
                    if (insIndex > 0) {
                        auto newArmor = CreateCoopItem_NB(npc, insIndex);
                        if (newArmor) {
                            lastArmor = insIndex;
                            npc->Equip(newArmor);
                        }
                    }

                }
            }
        }

        void UpdateHelmet(json update) {
            if (hasNpc && hasModel) {
                auto helmet = update["helmet"].get<std::string>();


                //cmd << "UpdateHelmet: " << update.dump().c_str() << endl;

                auto currentHelmet = npc->GetEquippedHelmet();
                if (currentHelmet) {
                    lastHelmet = -1;
                    npc->UnequipItem(currentHelmet);

                }


                if (helmet != "NULL") {

                    int insIndex = parser->GetIndex(helmet.c_str());
                    if (insIndex > 0) {
                        auto newHelmet = CreateCoopItem_NB(npc, insIndex);
                        if (newHelmet) {
                            lastHelmet = insIndex;

                            npc->Equip(newHelmet);
                        }
                    }

                }
            }
        }

        void UpdateShieldSlot(json update) {
            if (hasNpc && hasModel) {
                auto shieldSlotItemName = update["slotShield"].get<std::string>();

                auto currentItem = npc->GetSlotItem(NPC_NODE_SHIELD);

                zSTRING curUtem = currentItem ? currentItem->GetInstanceName() : "NULL";


                if (curUtem.ToChar() == shieldSlotItemName)
                {
                    //cmd << "EQUIALS" << endl;

                    return;
                }

               // cmd << "UpdateShieldSlot: " << curUtem << "|" << shieldSlotItemName.c_str() << endl;

                if (currentItem) {
                    lastShield = -1;
                    npc->UnequipItem(currentItem);
                }


                if (shieldSlotItemName != "NULL") {

                    int insIndex = parser->GetIndex(shieldSlotItemName.c_str());
                    if (insIndex > 0) {
                        auto newItem = CreateCoopItem_NB(npc, insIndex);
                        if (newItem) {
                            lastShield = insIndex;

                            npc->Equip(newItem);
                        }
                    }

                }
            }
        }

        void UpdateTorchSlot(json update) {
            if (hasNpc && hasModel) {
                auto leftItemName = update["slotTorch"].get<std::string>();

                auto currentItem = npc->GetSlotItem(NPC_NODE_TORCH_NB);

                if (currentItem) {
                    lastTorch = -1;
                    npc->RemoveFromSlot(NPC_NODE_TORCH_NB, 0, TRUE);
                }


                if (leftItemName != "NULL") {

                    int insIndex = parser->GetIndex(leftItemName.c_str());
                    if (insIndex > 0) {
                        auto newItem = CreateCoopItem_NB(npc, insIndex);
                        if (newItem) {
                            lastTorch = insIndex;

                            auto slot = npc->GetInvSlot(NPC_NODE_TORCH_NB);

                            if (!slot)
                            {
                                npc->CreateInvSlot(NPC_NODE_TORCH_NB);
                            }

                            npc->PutInSlot(NPC_NODE_TORCH_NB, newItem, 0);
                        }
                    }

                }
            }
        }

        void UpdateThighSlot(json update) {
            if (hasNpc && hasModel) {
                auto thighItem = update["slotThigh"].get<std::string>();

                auto currentItem = npc->GetSlotItem(NPC_NODE_THIGH_NB);

                if (currentItem) {
                    lastThigh = -1;
                    npc->RemoveFromSlot(NPC_NODE_THIGH_NB, 0, TRUE);
                }


                if (thighItem != "NULL") {

                    int insIndex = parser->GetIndex(thighItem.c_str());
                    if (insIndex > 0) {
                        auto newItem = CreateCoopItem_NB(npc, insIndex);
                        if (newItem) {
                            lastThigh = insIndex;

                            auto slot = npc->GetInvSlot(NPC_NODE_THIGH_NB);

                            if (!slot)
                            {
                                npc->CreateInvSlot(NPC_NODE_THIGH_NB);
                            }

                            npc->PutInSlot(NPC_NODE_THIGH_NB, newItem, 0);
                        }
                    }

                }
            }
        }

        void UpdateTrophySlot(json update) {
            if (hasNpc && hasModel) {
                auto trophyItemName = update["slotTrophy"].get<std::string>();

                auto currentItem = npc->GetSlotItem(NPC_NODE_TROPHY_NB);

                if (currentItem) {
                    lastTrophy = -1;
                    npc->RemoveFromSlot(NPC_NODE_TROPHY_NB, 0, TRUE);
                }


                if (trophyItemName != "NULL") {

                    int insIndex = parser->GetIndex(trophyItemName.c_str());
                    if (insIndex > 0) {
                        auto newItem = CreateCoopItem_NB(npc, insIndex);
                        if (newItem) {
                            lastTrophy = insIndex;

                            auto slot = npc->GetInvSlot(NPC_NODE_TROPHY_NB);

                            if (!slot)
                            {
                                npc->CreateInvSlot(NPC_NODE_TROPHY_NB);
                            }

                            npc->PutInSlot(NPC_NODE_TROPHY_NB, newItem, 0);
                        }
                    }

                }
            }
        }

        void UpdateLeftHandSlot(json update) {
            if (hasNpc && hasModel) {
                auto leftItemName = update["slotLeftHand"].get<std::string>();

               // cmd << "Update left arm: " << leftItemName.c_str() << endl;

                auto currentItem = npc->GetSlotItem(NPC_NODE_LEFTHAND);

                zSTRING curUtem = currentItem ? currentItem->GetInstanceName() : "NULL";


                if (curUtem.ToChar() == leftItemName)
                {
                   // cmd << "EQUIALS LEFT ARM" << endl;

                    return;
                }


                if (currentItem) {
                    lastLeftHand = -1;
                    npc->RemoveFromSlot(NPC_NODE_LEFTHAND, 0, TRUE);
                }


                //cmd << "UpdateLeftHandSlot: " << curUtem << "|" << leftItemName.c_str() << endl;

                if (leftItemName != "NULL") {

                    int insIndex = parser->GetIndex(leftItemName.c_str());
                    if (insIndex > 0) {
                        auto newItem = CreateCoopItem_NB(npc, insIndex);
                        if (newItem) {
                            lastLeftHand = insIndex;

                            auto slot = npc->GetInvSlot(NPC_NODE_LEFTHAND);

                            if (!slot)
                            {
                                npc->CreateInvSlot(NPC_NODE_LEFTHAND);
                            }

                            npc->PutInSlot(NPC_NODE_LEFTHAND, newItem, 0);
                        }
                    }

                }
            }
        }
        
        void UpdateRightHandSlot(json update) {
            if (hasNpc && hasModel) {
                auto rightItemName = update["slotRightHand"].get<std::string>();

                auto currentItem = npc->GetSlotItem(NPC_NODE_RIGHTHAND);

                zSTRING curUtem = currentItem ? currentItem->GetInstanceName() : "NULL";

                if (curUtem.ToChar() == rightItemName)
                {
                    //cmd << "EQUIALS RIGHT HAND" << endl;

                    return;
                }


                if (currentItem) {
                    lastRightHand = -1;
                    npc->RemoveFromSlot(NPC_NODE_RIGHTHAND, 0, TRUE);
                }


                if (rightItemName != "NULL") {

                    int insIndex = parser->GetIndex(rightItemName.c_str());
                    if (insIndex > 0) {
                        auto newItem = CreateCoopItem_NB(npc, insIndex);
                        if (newItem) {
                            lastRightHand = insIndex;

                            auto slot = npc->GetInvSlot(NPC_NODE_RIGHTHAND);

                            if (!slot)
                            {
                                npc->CreateInvSlot(NPC_NODE_RIGHTHAND);
                            }

                            npc->PutInSlot(NPC_NODE_RIGHTHAND, newItem, 0);
                        }
                    }

                }
            }
        }
        

        void UpdateBodystate(json update) {
            if (hasNpc) {
                auto bs = update["bs"].get<int>();
                npc->SetBodyState(bs);
            }
        }

        void UpdateOverlays(json update) {
            if (hasNpc) {
                std::vector<json> overlays = update["overlays"];
                zCArray<int> overlaysNew;

                for each (auto a in overlays) {
                    auto overlayId = a["over"].get<int>();
                    overlaysNew.InsertEnd(overlayId);
                }

                if (!npc->CompareOverlaysArray(overlaysNew))
                {
                    npc->ApplyOverlaysArray(overlaysNew);
                }
            }
        }

        void UpdateVisual(json update) {
            if (hasNpc) {
                
                auto newNpcInst = update["newVisual"].get<std::string>();
                bool isHeroReturn = false;

                if (newNpcInst == "PC_HERO")
                {
                    newNpcInst = FriendInstanceId;
                    isHeroReturn = true;
                }

                

                auto pos = npc->GetPositionWorld();

                auto uniqName = GetNpcUniqName(npc);

                cmd << "NewVisual: " << newNpcInst.c_str() << " isHeroRet: " << isHeroReturn << " uniq: " << uniqName << endl;


                if (UniqueNameToNpcList.count(uniqName))
                {
                    UniqueNameToNpcList.erase(uniqName);
                }
                else
                {
                    //cmd << "No uniq name: " << endl;
                   // return;
                }

                if (NpcToUniqueNameList.count(npc))
                {
                    NpcToUniqueNameList.erase(npc);
                }

                //npc->trafo->SetTranslation(zVEC3(0, 0, 0));

                if (npc->GetWeaponMode() == NPC_WEAPON_MAG)
                {
                    oCMag_Book* mbook = npc->GetSpellBook();
                    if (mbook) mbook->KillSelectedSpell();
                    npc->EV_ForceRemoveWeapon(NULL);

                    npc->KillActiveSpells();
                }
                
                npc->dontWriteIntoArchive = true;

              
                cmd << "RemoveVob npc #1: " << npc->GetInstanceName() << " hw: " << (int)npc->homeWorld << " npc: " << (int)npc << endl;
                ogame->GetGameWorld()->RemoveVob(npc);
                cmd << "RemoveVob npc #2: " << npc->GetInstanceName() << " hw: " << (int)npc->homeWorld << " npc: " << (int)npc << endl;



                if (isHeroReturn)
                {
                    npc = NULL;

                   cmd << "Init npc hero" << endl;
                    Reset();
                    InitCoopFriendNpc();
                    UpdateHasNpcAndHasModel();
                }
                else
                {

                    int index = parser->GetIndex(newNpcInst.c_str());

                    if (index != -1)
                    {
                        cmd << "Some transrom" << endl;

                        SaveParserVars();
                        oCNpc* newNpc = ogame->GetSpawnManager()->SummonNpc(index, pos, 0.0f);
                        newNpc->dontWriteIntoArchive = true;
                        RestoreParserVars();

                        npc = newNpc;

                        // restore player name
                        npc->name[0] = playerNickName;

                        // transform monster will use no AI :)
                        npc->senses_range = 0;
                        npc->UseStandAI();
                        //NpcToUniqueNameList[npc] = uniqName;
                       // UniqueNameToNpcList[uniqName] = npc;


                        UpdateHasNpcAndHasModel();
                    }
                }
                
            }
        }
        

        void UpdateMagicSetup(json update) {
            if (hasNpc && hasModel) {
                auto spellInstanceName = update["spell"].get<std::string>();
                oCMag_Book* book = npc->GetSpellBook();
                if (book)
                {
                    auto selectedSpell = book->GetSelectedSpell();
                    if (selectedSpell) {
                        auto selectedSpellItem = book->GetSpellItem(selectedSpell);
                        npc->DoDropVob(selectedSpellItem);
                        selectedSpellItem->RemoveVobFromWorld();
                        selectedSpell->Kill();
                    }

                    book->spellitems.EmptyList();
                    book->spells.EmptyList();
                }

                if (spellInstanceName.compare("NULL") != 0) {
                    int insIndex = parser->GetIndex(spellInstanceName.c_str());
                    if (insIndex > 0) {
                        auto spellItem = CreateCoopItem(insIndex);
                        if (spellItem) {
                            npc->DoPutInInventory(spellItem);
                            npc->Equip(spellItem);

                            oCMag_Book* book = npc->GetSpellBook();
                            if (book) {
                                book->Open(0);
                            }
                        }
                    }
                }
            }
        }

        void UpdateHand(json update) {
            if (!hasModel) {
                return;
            }
            auto leftItem = update["left"].get<std::string>();
            auto rightItem = update["right"].get<std::string>();

            auto leftHandItem = npc->GetLeftHand();
            if (leftHandItem)
            {
                npc->DoDropVob(leftHandItem);
                leftHandItem->RemoveVobFromWorld();
                syncedNpcItems.erase(leftHandItem);
            }

            if (leftItem != "NULL") {
                int insIndex = parser->GetIndex(leftItem.c_str());
                if (insIndex > 0) {
                    auto newItem = CreateCoopItem(insIndex);
                    if (newItem) {
                        syncedNpcItems[newItem] = true;
                        npc->SetLeftHand(newItem);
                    }
                }
            }

            auto rightHandItem = npc->GetRightHand();
            if (rightHandItem)
            {
                npc->DoDropVob(rightHandItem);
                rightHandItem->RemoveVobFromWorld();
                syncedNpcItems.erase(rightHandItem);
            }

            if (rightItem != "NULL") {
                int insIndex = parser->GetIndex(rightItem.c_str());
                if (insIndex > 0) {
                    auto newItem = CreateCoopItem(insIndex);
                    if (newItem) {
                        syncedNpcItems[newItem] = true;
                        npc->SetRightHand(newItem);
                    }
                }
            }
        }


        void UpdateWeapon_Melee(zSTRING weapon1)
        {
            auto currentWeapon1 = npc->GetEquippedMeleeWeapon();

            zSTRING curUtem1 = currentWeapon1 ? currentWeapon1->GetInstanceName() : "NULL";

            if (curUtem1 == weapon1)
            {
                return;
            }

            if (currentWeapon1) {
                npc->UnequipItem(currentWeapon1);
                lastWeapon1 = -1;
            }

            if (weapon1 != "NULL") {
                int insIndex = parser->GetIndex(weapon1);
                if (insIndex > 0) {
                    auto newWeapon = CreateCoopItem(insIndex);
                    if (newWeapon) {
                        lastWeapon1 = insIndex;
                        npc->Equip(newWeapon);
                        //syncedNpcItems[newWeapon] = true;
                    }
                }

            }
        }

        void UpdateWeapon_Range(zSTRING weapon2)
        {
            auto currentWeapon2 = npc->GetEquippedRangedWeapon();

            zSTRING curUtem2 = currentWeapon2 ? currentWeapon2->GetInstanceName() : "NULL";

            if (curUtem2 == weapon2)
            {
                return;
            }

            if (currentWeapon2) {
                npc->UnequipItem(currentWeapon2);
                lastWeapon2 = -1;
            }

            if (weapon2 != "NULL") {
                int insIndex = parser->GetIndex(weapon2);
                if (insIndex > 0) {
                    auto newWeapon = CreateCoopItem(insIndex);
                    if (newWeapon) {
                        lastWeapon2 = insIndex;
                        npc->Equip(newWeapon);
                        //syncedNpcItems[newWeapon] = true;
                    }
                }

            }
        }

        void UpdateWeapons(json update) {
            if (hasModel) {
                auto weapon1 = update["w1"].get<std::string>();
                auto weapon2 = update["w2"].get<std::string>();


                UpdateWeapon_Melee(weapon1.c_str());
                UpdateWeapon_Range(weapon2.c_str());
              
            }
        }

        void UpdateDropItem(json update) {
            if (!hasModel) {
                return;
            }

            auto itemName = update["itemDropped"].get<std::string>();
            auto count = update["count"].get<int>();
            auto flags = update["flags"].get<int>();
            auto itemUniqName = update["itemUniqName"].get<std::string>();

            
            int index = parser->GetIndex(itemName.c_str());

            if (index != -1)
            {
                npc->DropItemByIndex(index, flags, count, Z itemUniqName.c_str());
            }

        }

        void UpdateTakeItem(json update) {
            if (!hasModel) {
                return;
            }

            auto str = update.dump();

            //cmd << "UpdateTakeItem: " << str.c_str() << endl;

            auto itemName = update["itemDropped"].get<std::string>();
            auto count = update["count"].get<int>();
            auto flags = update["flags"].get<int>();
            auto x = update["x"].get<float>();
            auto y = update["y"].get<float>();
            auto z = update["z"].get<float>();
            auto uniqName = update["uniqName"].get<std::string>();
            auto itemPos = zVEC3(x, y, z);


            auto pList = CollectVobsInRadius(itemPos, 2500);

            int index = parser->GetIndex(itemName.c_str());

            if (index == -1) return;

           // cmd << "List is ready: " << pList.GetNumInList() << endl;

            for (int i = 0; i < pList.GetNumInList(); i++)
            {
                if (auto pVob = pList.GetSafe(i))
                {
                    if (auto pItem = pVob->CastTo<oCItem>())
                    {
                        if (pItem->GetInstance() == index && pItem->GetObjectName() == uniqName.c_str())
                        {
                            //cmd << "Item Found!" << endl;
                            //NB_PrintWin("REmove item!");
                            pItem->RemoveVobFromWorld();
                            break;
                        }
                    }
                }
            }

           // cmd << "UpdateTakeItem end" << endl;
        }
        

        void UpdateSpellCasts(json update) {
            if (!hasModel) {
                return;
            }

            // cmd << "UpdateSpellCasts: " << update.dump().c_str() << " npc: " << npc->GetInstanceName() << endl;

            oCMag_Book* book = npc->GetSpellBook();
            if (book)
            {
                auto selectedSpell = book->GetSelectedSpell();
                if (selectedSpell) {
                    std::vector<json> casts = update["casts"];
                    for each (auto c in casts) {

                        std::string target = c["target"].get<std::string>();


                        if (target.length() > 0)
                        {
                            if (!target.empty() && UniqueNameToNpcList.count(target.c_str()) > 0) {

                                oCNpc* targetNpc = UniqueNameToNpcList[target.c_str()];

                                if (targetNpc)
                                {
                                   
                                   // cmd << "TargetNpc: " << targetNpc->GetInstanceName() << " spells: " << Z book->spells.GetNum()  << endl;


                                    book->Spell_Setup(0, npc, UniqueNameToNpcList[target.c_str()]);
                                }
                                else
                                {
                                    //cmd << "target is NULL" << endl;
                                }
                            }
                            else {
                                zCVob* nullVob = NULL;

                                book->Spell_Setup(0, npc, nullVob);
                            }

                            book->Spell_Invest();

                            //cmd << "Name: " << book->GetSelectedSpell()->GetName() << endl;

                            if (book->GetSelectedSpell() && !IsSummonRune(book->GetSelectedSpell()->spellID))
                            {
                                book->Spell_Cast();
                            }
                            
                        }


                        
                    }
                }
            }
        }

        void UpdateAttacks(json update) {
            if (!hasModel) {
                return;
            }

            std::vector<json> atts = update["att"];
            for each (auto a in atts) {
                auto target = a["target"].get<std::string>();
                float damage = a["damage"].get<float>();
                auto isUnconscious = a["isUnconscious"].get<int>();
                auto stillAlive = !a["isDead"].get<bool>();
                auto damageMode = a["damageMode"].get<unsigned long>();

                // attack player (client only, eg. wolf attacks player)
                if (target.compare(MyselfId) == 0) {
                    auto targetNpc = player;
                    int health = targetNpc->GetAttribute(NPC_ATR_HITPOINTS);

                    if (isUnconscious && stillAlive) {
                        targetNpc->SetWeaponMode2(NPC_WEAPON_NONE);
                        targetNpc->DropUnconscious(1, npc);
                    }

                    if (stillAlive) {
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, 999999);
                        targetNpc->GetEM(false)->OnDamage(targetNpc, npc, COOP_MAGIC_NUMBER, damageMode, targetNpc->GetPositionWorld());
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, health - damage);
                    }
                    else {
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, 1);
                        targetNpc->GetEM(false)->OnDamage(targetNpc, npc, COOP_MAGIC_NUMBER, damageMode, targetNpc->GetPositionWorld());
                    }

                    break;
                }

                // attack other coop player (client only, eg. wolf attacks host)
                if (IsCoopPlayer(target) && PlayerNameToNpc.count(target.c_str())) {
                    auto targetNpc = PlayerNameToNpc[target.c_str()];
                    int health = targetNpc->GetAttribute(NPC_ATR_HITPOINTS);

                    if (isUnconscious && stillAlive) {
                        targetNpc->SetWeaponMode2(NPC_WEAPON_NONE);
                        targetNpc->DropUnconscious(1, npc);
                    }

                    if (stillAlive) {
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, 999999);
                        targetNpc->GetEM(false)->OnDamage(targetNpc, npc, 1, damageMode, targetNpc->GetPositionWorld());
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, health);
                    }
                    else {
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, 0);
                        targetNpc->DoDie(npc);
                    }

                    break;
                }

                // attack any world npc (eg. client attacks Moe, Cavalorn attacks goblin, wolf attacks sheep)
                auto targetNpc = UniqueNameToNpcList[target.c_str()];
                if (targetNpc) {
                    int health = targetNpc->GetAttribute(NPC_ATR_HITPOINTS);
                    auto isTalkingWith = IsPlayerTalkingWithNpc(targetNpc);

                    if (isUnconscious && stillAlive) {
                        targetNpc->SetWeaponMode2(NPC_WEAPON_NONE);
                        targetNpc->DropUnconscious(1, npc);
                    }

                    if (stillAlive) {
                        if (!isTalkingWith && !isUnconscious) {
                            targetNpc->SetAttribute(NPC_ATR_HITPOINTS, 999999);
                            targetNpc->GetEM(false)->OnDamage(targetNpc, npc, 1, damageMode, targetNpc->GetPositionWorld());
                        }
                        if (ServerThread) {
                            targetNpc->SetAttribute(NPC_ATR_HITPOINTS, health - damage > 0 ? health - damage : 0);
                        }
                    }
                    else {
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, 1);

                        static int AIV_PARTYMEMBER = GetPartyMemberID();
                        if (IsCoopPlayer(npc->GetObjectName()) || npc->aiscriptvars[AIV_PARTYMEMBER] == True) {
                            targetNpc->OnDamage(targetNpc, player, COOP_MAGIC_NUMBER, damageMode, targetNpc->GetPositionWorld());
                        }

                        lastHpFromServer = -1;
                        targetNpc->SetAttribute(NPC_ATR_HITPOINTS, 0);
                    }
                }
            }
        }

        void UpdateTime(json update) {
            if (!IsPlayerTalkingWithAnybody()) {
                auto h = update["h"].get<int>();
                auto m = update["m"].get<int>();
                ogame->GetWorldTimer()->SetTime(h, m);
            }
        }

        void UpdateRevived(json update) {
            auto name = update["name"].get<std::string>();

            if (player->IsDead() && name.compare(MyselfId) == 0) {
                player->StopFaceAni("T_HURT");
                player->SetWeaponMode2(NPC_WEAPON_NONE);
                player->ResetPos(player->GetPositionWorld());
                player->SetAttribute(NPC_ATR_HITPOINTS, 1);
                parser->CallFuncByName("RX_Mult_ReviveHero");
            }
        }

        void DestroyNpc() {
            if (npc != NULL) {
                PlayerNpcs.erase(npc);
                PlayerNameToNpc.erase(name);
                remoteSummons.RemoveOrder(npc);

                SaveParserVars();
                ogame->spawnman->DeleteNpc(npc);
                RestoreParserVars();

                


                destroyed = true;
                npc = NULL;
            }
        }

        void UpdateNpcBasedOnLastDataFromServer() {
            if (npc && hasModel) {
                if (lastPositionFromServer.Distance(zVEC3(0, 0, 0)) >= 10) {
                    UpdateNpcPosition();
                }

                if (lastHpFromServer != -1 && lastHpFromServer != npc->GetAttribute(NPC_ATR_HITPOINTS)) {
                    if (!npc->IsDead()) {
                        npc->SetAttribute(NPC_ATR_HITPOINTS, lastHpFromServer);
                    }
                    else if (IsCoopPlayer(name)) {
                        npc->SetAttribute(NPC_ATR_HITPOINTS, lastHpFromServer);
                    }
                }


                if (lastWeaponMode != -1 && lastWeaponMode != npc->GetWeaponMode()) {
                    npc->SetWeaponMode2(lastWeaponMode);
                }

                if (lastHeadingFromServer != -1) {
                    float currentHeading = GetHeading(npc);
                    if (abs(currentHeading - lastHeadingFromServer) > 1) {
                        npc->ResetRotationsWorld();
                        npc->RotateWorldY(lastHeadingFromServer);
                    }
                }

                if (IsCoopPlayer(name)) {
                    static int AIV_PARTYMEMBER = GetPartyMemberID();
                    npc->aiscriptvars[AIV_PARTYMEMBER] = True;
                }
            }
        }

        void UpdateNpcPosition() {
            bool inMove = npc->isInMovementMode;
            if (inMove) {
#if ENGINE >= Engine_G2
                npc->EndMovement(false);
#else
                npc->EndMovement();
#endif
            }

            static int counter = 0;

            counter++;


            auto pos = lastPositionFromServer;

            // real and current pos
            auto dist = pos.Distance(npc->GetPositionWorld());

           // NB_PrintWin(zSTRING(dist).ToChar());
            zVEC3 newPos;


            if (dist <= 300)
            {
                if (Npc_IsRemoteSummon(npc))
                {
                    newPos = Lerp(npc->GetPositionWorld(), pos, 0.1f);
                }
                else
                {
                    // interpolateValue == 1.0f => no interpolation, can be set via json config
                    newPos = Lerp(npc->GetPositionWorld(), pos, interpolateValue);
                }
            }
            else
            {
                newPos = pos;
            }
            

            npc->trafoObjToWorld.SetTranslation(newPos);

            if (inMove) {
                npc->BeginMovement();
            }
        }

        void UpdateHasNpcAndHasModel() {
            hasNpc = npc != NULL;
            hasModel = npc && npc->GetModel() && npc->vobLeafList.GetNum() > 0;
        }

        void InitCoopFriendNpc() {
            int instanceId = GetFriendDefaultInstanceId();
            if (instanceId <= 0) {
                ChatLog("Invalid NPC instance id.");
                return;
            }
            if (!npc) {
                npc = dynamic_cast<oCNpc*>(ogame->GetGameWorld()->CreateVob(zTVobType::zVOB_TYPE_NSC, instanceId));
                //cmd << "Init with CREATE VOB: " << npc->GetInstanceName() << endl;
            }

            cmd << "InitCoopFriendNpc: " << npc->GetInstanceName() << ";" << npcInstName << endl;


            ogame->spawnman->InsertNpc(npc, lastPositionFromServer);
            isSpawned = true;
            npc->name[0] = playerNickname.IsEmpty() ? zSTRING(name) : playerNickname;

            npc->SetObjectName(name);
            npc->SetVobName(name);
            npc->SetVobPresetName(name);
            npc->MakeSpellBook();


#ifndef NEWBALANCE_MOD
            npc->UseStandAI();
#endif // NEWBALANCE_MOD
            
            npc->dontWriteIntoArchive = TRUE;
            //npc->idx = 69133769;

#if ENGINE >= Engine_G2
            npc->SetHitChance(1, 100);
            npc->SetHitChance(2, 100);
            npc->SetHitChance(3, 100);
            npc->SetHitChance(4, 100);
#endif
            npc->SetAttribute(NPC_ATR_STRENGTH, COOP_MAGIC_NUMBER);
            npc->SetAttribute(NPC_ATR_DEXTERITY, COOP_MAGIC_NUMBER);
            npc->SetAttribute(NPC_ATR_MANA, 10000);
            npc->SetAttribute(NPC_ATR_MANAMAX, 10000);

            auto armor = npc->GetEquippedArmor();
            if (armor) {
                npc->UnequipItem(armor);
            }

            auto helmet = npc->GetEquippedHelmet();

            if (helmet) {
                npc->UnequipItem(helmet);
            }

            auto weapon1 = npc->GetEquippedMeleeWeapon();
            if (weapon1) {
                npc->UnequipItem(weapon1);
            }

            auto weapon2 = npc->GetEquippedRangedWeapon();
            if (weapon2) {
                npc->UnequipItem(weapon2);
            }

            auto shieldItem = npc->GetSlotItem(NPC_NODE_SHIELD);

            if (shieldItem){
                npc->UnequipItem(shieldItem);
            }

            auto torchItem = npc->GetSlotItem(NPC_NODE_TORCH_NB);

            if (torchItem) {
                npc->RemoveFromSlot(NPC_NODE_TORCH_NB, 0, TRUE);
            }

            auto trophyItem = npc->GetSlotItem(NPC_NODE_TROPHY_NB);

            if (trophyItem) {
                npc->RemoveFromSlot(NPC_NODE_TROPHY_NB, 0, TRUE);
            }


            if (lastWeapon1 > 0) {
                auto weapon = CreateCoopItem(lastWeapon1);
                npc->Equip(weapon);
            }

            if (lastWeapon2 > 0) {
                auto weapon = CreateCoopItem(lastWeapon2);
                npc->Equip(weapon);
            }

            if (lastArmor > 0) {
                auto armor = CreateCoopItem_NB(npc, lastArmor);
                npc->Equip(armor);
            }

            if (lastHelmet > 0) {
                auto helmet = CreateCoopItem_NB(npc, lastHelmet);
                npc->Equip(helmet);
            }

            if (lastShield > 0) {
                auto shield = CreateCoopItem_NB(npc, lastShield);
                npc->Equip(shield);
            }
            
            if (lastTorch > 0) {
                auto torchItem = CreateCoopItem_NB(npc, lastTorch);

                auto slot = npc->GetInvSlot(NPC_NODE_TORCH_NB);

                if (!slot)
                {
                    npc->CreateInvSlot(NPC_NODE_TORCH_NB);
                }

                npc->PutInSlot(NPC_NODE_TORCH_NB, torchItem, 0);
      
            }

            if (lastTrophy > 0) {
                auto trophyItem = CreateCoopItem_NB(npc, lastTrophy);

                auto slot = npc->GetInvSlot(NPC_NODE_TROPHY_NB);

                if (!slot)
                {
                    npc->CreateInvSlot(NPC_NODE_TROPHY_NB);
                }

                npc->PutInSlot(NPC_NODE_TROPHY_NB, trophyItem, 0);

            }

            if (lastThigh > 0) {
                auto thighItem = CreateCoopItem_NB(npc, lastThigh);

                auto slot = npc->GetInvSlot(NPC_NODE_THIGH_NB);

                if (!slot)
                {
                    npc->CreateInvSlot(NPC_NODE_THIGH_NB);
                }

                npc->PutInSlot(NPC_NODE_THIGH_NB, thighItem, 0);

            }

            if (lastLeftHand > 0) {
                auto leftItem = CreateCoopItem_NB(npc, lastLeftHand);

                auto slot = npc->GetInvSlot(NPC_NODE_LEFTHAND);

                if (!slot)
                {
                    npc->CreateInvSlot(NPC_NODE_LEFTHAND);
                }

                npc->PutInSlot(NPC_NODE_LEFTHAND, leftItem, 0);

            }

            if (lastRightHand > 0) {
                auto rightItem = CreateCoopItem_NB(npc, lastRightHand);

                auto slot = npc->GetInvSlot(NPC_NODE_RIGHTHAND);

                if (!slot)
                {
                    npc->CreateInvSlot(NPC_NODE_RIGHTHAND);
                }

                npc->PutInSlot(NPC_NODE_RIGHTHAND, rightItem, 0);

            }


            if (lastWeaponMode > 0) {
                npc->SetWeaponMode2(lastWeaponMode);
            }

            if (IsCoopPlayer(name)) {
                static int AIV_PARTYMEMBER = GetPartyMemberID();
                npc->aiscriptvars[AIV_PARTYMEMBER] = True;
            }

            PlayerNpcs[npc] = name;
            PlayerNameToNpc[name] = npc;
        }

        void RemoveCoopItemsFromGround() {
            if (hasNpc && syncedNpcItems.size() > 0) {
                auto rightItem = npc->GetRightHand();
                auto leftItem = npc->GetLeftHand();
                auto currentWeapon1 = npc->GetEquippedMeleeWeapon();
                auto currentWeapon2 = npc->GetEquippedRangedWeapon();

                for (auto syncedItem : syncedNpcItems) {
                    auto item = syncedItem.first;

                    if (!item) {
                        syncedNpcItems.erase(item);
                    }

                    if (item != leftItem && item != rightItem && item != currentWeapon1 && item != currentWeapon2) {
                        item->RemoveVobFromWorld();
                        syncedNpcItems.erase(item);
                    }
                }
            }
        }

        void RespawnOrDestroyBasedOnDistance() {
            if (hasNpc) {
                auto dist = (int)(lastPositionFromServer - player->GetPositionWorld()).LengthApprox();

                if (IsCoopPlayer(name)) {
                    if (dist > BROADCAST_DISTANCE * 1.1f && isSpawned) {

                        /*
                        SaveParserVars();
                        ogame->spawnman->DeleteNpc(npc);
                        RestoreParserVars();

                        cmd << "Npc deleted: " << npc->GetInstanceName() << ";" << npcInstName << endl;
                        npc = NULL;
                        isSpawned = false;
                        */
                    }

                    if (dist < BROADCAST_DISTANCE && (!isSpawned || !hasModel)) {
                        cmd << "Npc init in respawncheck: " << npc->GetInstanceName() << ";" << npcInstName << endl;

                        InitCoopFriendNpc();
                        isSpawned = true;

                       
                        if (Myself) {
                            Myself->Reinit();
                        }

                        for each (auto n in BroadcastNpcs)
                        {
                            n.second->Reinit();
                        }
                        
                    }
                }
                else if (dist > BROADCAST_DISTANCE * 1.5) {

                    //cmd << "Destoyed: " << npc->GetInstanceName() << endl;
                    destroyed = true;
                    return;
                }
                else if (dist < BROADCAST_DISTANCE && !hasModel) {

                   // cmd << "dist < BROADCAST_DISTANCE: " << npc->GetInstanceName() << endl;
                    ogame->spawnman->InsertNpc(npc, lastPositionFromServer);
                }
            }
        }

        bool IsNpcInTot() {
            if (CurrentWorldTOTPosition && npc && !IsCoopPlayer(name)) {
                auto newPosition = npc->GetPositionWorld();
                auto totPos = *CurrentWorldTOTPosition;
                auto dist = newPosition.Distance(totPos);
                if (dist < 500) {
                    destroyed = true;
                    return true;
                }
            }

            return false;
        }
    };
}
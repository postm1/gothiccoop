namespace GOTHIC_ENGINE {
    int LastHpBeforeDamage = -1;
    oCNpc::oSDamageDescriptor* LastIgnoredDamDesc = NULL;



    HOOK ivk_oCNpc_OnDamage_Hit AS(&oCNpc::OnDamage_Hit, &oCNpc::OnDamage_Hit_Union);

    void oCNpc::OnDamage_Hit_Union(oSDamageDescriptor& damdesc)
    {


        cmd << this->GetInstanceName() << " got hit with " << damdesc.fDamageTotal << endl;

        if (damdesc.pNpcAttacker == player && IsCoopPlayer(this->GetObjectName())) {
            return;
        }

        if (IsCoopPaused) {
            THISCALL(ivk_oCNpc_OnDamage_Hit)(damdesc);
            return;
        }

        // Blocking animations on clients for NPC is not preventing attacking sometimes (so do not call unless attack from the coop engine)
        if (ClientThread && damdesc.pNpcAttacker != player && this == player && damdesc.fDamageTotal != COOP_MAGIC_NUMBER) {
            return;
        }


        if (ClientThread && damdesc.pNpcAttacker != player) {
            THISCALL(ivk_oCNpc_OnDamage_Hit)(damdesc);
            return;
        }

        if (ServerThread && PlayerNpcs.count(damdesc.pNpcAttacker)) {
            THISCALL(ivk_oCNpc_OnDamage_Hit)(damdesc);
            return;
        }

        if (damdesc.pNpcAttacker == player && damdesc.fDamageTotal == COOP_MAGIC_NUMBER) {
            LastIgnoredDamDesc = &damdesc;
            THISCALL(ivk_oCNpc_OnDamage_Hit)(damdesc);
            return;
        }

        float HpMultipler = 1.0;
        if (damdesc.pNpcAttacker == player) {
            HpMultipler = PlayersDamageMultipler / 100.0;
        }
        else if (this == player) {
            HpMultipler = NpcsDamageMultipler / 100.0;
        }

        int HpBeforeOnDamage = this->GetAttribute(NPC_ATR_HITPOINTS);
        int MaxHpBeforeOnDamage = this->GetAttribute(NPC_ATR_HITPOINTSMAX);
        LastHpBeforeDamage = HpBeforeOnDamage;


        //fixme
        //if (HpMultipler == 1.0) 
        {
            THISCALL(ivk_oCNpc_OnDamage_Hit)(damdesc);
            return;
        }
        //=============================================================
        this->SetAttribute(NPC_ATR_HITPOINTS, 100000);
        this->SetAttribute(NPC_ATR_HITPOINTSMAX, 100000);

        THISCALL(ivk_oCNpc_OnDamage_Hit)(damdesc);

        int RealDamage = (100000 - this->GetAttribute(NPC_ATR_HITPOINTS));
        int HpDamage = RealDamage * HpMultipler;

        this->SetAttribute(NPC_ATR_HITPOINTS, HpBeforeOnDamage - HpDamage);
        this->SetAttribute(NPC_ATR_HITPOINTSMAX, MaxHpBeforeOnDamage);
    }

    void __fastcall oCNpc_OnDamage_Sound(oCNpc*, void*, oCNpc::oSDamageDescriptor&);
#if ENGINE >= Engine_G2
    CInvoke<void(__thiscall*)(oCNpc*, oCNpc::oSDamageDescriptor&)> Ivk_oCNpc_OnDamage_Sound(0x0067A8A0, &oCNpc_OnDamage_Sound);
#else
    CInvoke<void(__thiscall*)(oCNpc*, oCNpc::oSDamageDescriptor&)> Ivk_oCNpc_OnDamage_Sound(0x00746660, &oCNpc_OnDamage_Sound);
#endif
    void __fastcall oCNpc_OnDamage_Sound(oCNpc* _this, void* vtable, oCNpc::oSDamageDescriptor& damdesc) {
        if (damdesc.pNpcAttacker == player && IsCoopPlayer(_this->GetObjectName())) {
            return;
        }

        if (LastIgnoredDamDesc == &damdesc) {
            Ivk_oCNpc_OnDamage_Sound(_this, damdesc);
            LastIgnoredDamDesc = NULL;
            return;
        }

        if (IsCoopPaused) {
            Ivk_oCNpc_OnDamage_Sound(_this, damdesc);
            return;
        }

        bool IsFinishUnconsciousHitWithOnlySoundFunctionCall = !damdesc.pNpcAttacker && damdesc.bIsDead;
        if (IsFinishUnconsciousHitWithOnlySoundFunctionCall) {
            Ivk_oCNpc_OnDamage_Sound(_this, damdesc);
            return;
        }

        // Blocking animations on clients for NPC is not preventing attacking sometimes (so do not call unless attack from the coop engine)
        if (ClientThread && damdesc.pNpcAttacker != player && _this == player && damdesc.fDamageTotal != COOP_MAGIC_NUMBER) {
            return;
        }


        if (ClientThread && damdesc.pNpcAttacker != player && !Npc_IsSummon(damdesc.pNpcAttacker)) {
            Ivk_oCNpc_OnDamage_Sound(_this, damdesc);
            return;
        }

        if (ServerThread && PlayerNpcs.count(damdesc.pNpcAttacker)) {
            Ivk_oCNpc_OnDamage_Sound(_this, damdesc);
            return;
        }

        if ((damdesc.pNpcAttacker == player || Npc_IsSummon(damdesc.pNpcAttacker)) && damdesc.fDamageTotal == COOP_MAGIC_NUMBER) {
            Ivk_oCNpc_OnDamage_Sound(_this, damdesc);
            return;
        }

        PlayerHit hit;
        hit.damage = LastHpBeforeDamage - _this->GetAttribute(NPC_ATR_HITPOINTS);
        hit.attacker = damdesc.pNpcAttacker;
        hit.npc = _this;
        hit.isDead = _this->IsDead();
        hit.isUnconscious = _this->IsUnconscious();
        hit.damageMode = damdesc.enuModeDamage;
        ReadyToSyncDamages.enqueue(hit);

        Ivk_oCNpc_OnDamage_Sound(_this, damdesc);
    }

    void __fastcall oCNpc_EV_AttackFinish(oCNpc*, void*, oCMsgAttack*);
#if ENGINE >= Engine_G2
    CInvoke<void(__thiscall*)(oCNpc*, oCMsgAttack*)> Ivk_oCNpc_EV_AttackFinish(0x00751AF0, &oCNpc_EV_AttackFinish);
#else
    CInvoke<void(__thiscall*)(oCNpc*, oCMsgAttack*)> Ivk_oCNpc_EV_AttackFinish(0x006AC180, &oCNpc_EV_AttackFinish);
#endif
    void __fastcall oCNpc_EV_AttackFinish(oCNpc* _this, void* vtable, oCMsgAttack* attack) {
        if (!ClientThread || _this != player) {
            Ivk_oCNpc_EV_AttackFinish(_this, attack);
            return;
        }

        auto model = _this->GetModel();
        auto hitAni = model->GetAniFromAniID(attack->hitAni);

        if (model && hitAni && model->IsAniActive(hitAni)) {
            float progress = model->GetProgressPercent(attack->hitAni);
            if (progress >= 0.5 && attack->target) {
                oCNpc* enemy = zDYNAMIC_CAST<oCNpc>(attack->target);
                if (enemy && enemy->IsUnconscious())
                {
                    //NB_PrintWin("Finish: " + _this->GetInstanceName());
                    PlayerHit hit;
                    hit.damage = 1;
                    hit.attacker = player;
                    hit.npc = enemy;
                    hit.isDead = true;
                    hit.isUnconscious = 0;
                    hit.damageMode = oETypeDamage::oEDamageType_Edge;
                    ReadyToSyncDamages.enqueue(hit);
                }
            }
        }

        Ivk_oCNpc_EV_AttackFinish(_this, attack);
    }

    int __fastcall oCAIHuman_StandActions(oCAIHuman*, void*);
#if ENGINE >= Engine_G2
    CInvoke<int(__thiscall*)(oCAIHuman*)> Ivk_oCAIHuman_StandActions(0x00698EA0, &oCAIHuman_StandActions);
#else
    CInvoke<int(__thiscall*)(oCAIHuman*)> Ivk_oCAIHuman_StandActions(0x00612840, &oCAIHuman_StandActions);
#endif
    int __fastcall oCAIHuman_StandActions(oCAIHuman* _this, void* vtable) {

        auto focusedNpc = player->GetFocusNpc();
        if (focusedNpc && IsCoopPlayer(focusedNpc->GetObjectName())) {
            return 0;
        }

        if (ServerThread) {
            return Ivk_oCAIHuman_StandActions(_this);
        }

        if (_this->npc != player) {
            return Ivk_oCAIHuman_StandActions(_this);
        }

        if (!focusedNpc) {
            return Ivk_oCAIHuman_StandActions(_this);
        }

        if (focusedNpc->IsDead() || focusedNpc->IsUnconscious() || focusedNpc->GetWeaponMode() != NPC_WEAPON_NONE) {
            return Ivk_oCAIHuman_StandActions(_this);
        }

        auto activeAnims = GetCurrentAni(focusedNpc);
        for (auto ani : activeAnims) {
            focusedNpc->GetModel()->StopAnimation(ani->aniName);
        }

        focusedNpc->GetEM()->KillMessages();
        focusedNpc->ClearEM();

        return Ivk_oCAIHuman_StandActions(_this);

    }

    void __fastcall oCMag_Book_Spell_Cast(oCMag_Book*, void*);
#if ENGINE >= Engine_G2
    CInvoke<void(__thiscall*)(oCMag_Book*)> Ivk_oCMag_Book_Spell_Cast(0x004767A0, &oCMag_Book_Spell_Cast);
#else
    CInvoke<void(__thiscall*)(oCMag_Book*)> Ivk_oCMag_Book_Spell_Cast(0x0046FC00, &oCMag_Book_Spell_Cast);
#endif
    void __fastcall oCMag_Book_Spell_Cast(oCMag_Book* _this, void* vtable) {
        auto castingNpc = (oCNpc*)_this->owner;

        if (IsCoopPaused) {
            Ivk_oCMag_Book_Spell_Cast(_this);
            return;
        }

        if (!castingNpc) {
            Ivk_oCMag_Book_Spell_Cast(_this);
            return;
        }

        oCMag_Book* book = castingNpc->GetSpellBook();
        if (!book) {
            Ivk_oCMag_Book_Spell_Cast(_this);
            return;
        }

        if (IsCoopPlayer(castingNpc->GetObjectName())) {
            int spellID = book->GetSelectedSpellNr();
            if (spellID >= 0)
            {
                oCItem* item = book->GetSpellItem(spellID);
                if (item)
                {
                    auto itemName = item->GetInstanceName();
                    int insIndex = parser->GetIndex(itemName);
                    if (insIndex > 0) {
                        auto spellItem = CreateCoopItem(insIndex);
                        if (spellItem) {
                            castingNpc->DoPutInInventory(spellItem);
                        }
                    }
                }
            }
        }

        Ivk_oCMag_Book_Spell_Cast(_this);

        SpellCast sc;
        sc.npc = (oCNpc*)_this->owner;
        sc.targetNpc = sc.npc->GetFocusNpc();
        ReadyToSyncSpellCasts.enqueue(sc);
    }

    void __fastcall oCNpc_OpenDeadNpc(oCNpc*, void*);
#if ENGINE >= Engine_G2
    CInvoke<void(__thiscall*)(oCNpc*)> Ivk_oCNpc_OpenDeadNpc(0x00762970, &oCNpc_OpenDeadNpc);
#else
    CInvoke<void(__thiscall*)(oCNpc*)> Ivk_oCNpc_OpenDeadNpc(0x006BB890, &oCNpc_OpenDeadNpc);
#endif
    void __fastcall oCNpc_OpenDeadNpc(oCNpc* _this, void* vtable) {
        auto focusNpc = _this->GetFocusNpc();

        if (focusNpc && IsCoopPlayer(focusNpc->GetObjectName())) {
            return;
        }

        Ivk_oCNpc_OpenDeadNpc(_this);
    }

    int __fastcall oCNpc_CanUse(oCNpc*, void*, oCItem*);
#if ENGINE >= Engine_G2
    CInvoke<int(__thiscall*)(oCNpc*, oCItem*)> Ivk_oCNpc_CanUse(0x007319B0, &oCNpc_CanUse);
#else
    CInvoke<int(__thiscall*)(oCNpc*, oCItem*)> Ivk_oCNpc_CanUse(0x0068EF00, &oCNpc_CanUse);
#endif
    int __fastcall oCNpc_CanUse(oCNpc* _this, void* vtable, oCItem* n) {
        if (IsCoopPlayer(_this->GetObjectName())) {
            return true;
        }

        return Ivk_oCNpc_CanUse(_this, n);
    }

    void __fastcall oCWorld_RemoveVob(oCWorld*, void*, zCVob*);
#if ENGINE >= Engine_G2
    CInvoke<void(__thiscall*)(oCWorld*, zCVob*)> Ivk_oCWorld_RemoveVob(0x007800C0, &oCWorld_RemoveVob);
#else
    CInvoke<void(__thiscall*)(oCWorld*, zCVob*)> Ivk_oCWorld_RemoveVob(0x006D6EF0, &oCWorld_RemoveVob);
#endif
    void __fastcall oCWorld_RemoveVob(oCWorld* _this, void* vtable, zCVob* vob) {
        if (IsSavingGame) {
            return Ivk_oCWorld_RemoveVob(_this, vob);
        }

        if (vob->GetCharacterClass() == zCVob::zTVobCharClass::zVOB_CHAR_CLASS_NPC) {
            auto npc = (oCNpc*)vob;

            if (npc && !IsCoopPlayer(npc->GetObjectName())) {
                if (NpcToUniqueNameList.count(npc) > 0) {
                    auto uniqueName = NpcToUniqueNameList[npc];
                    if (BroadcastNpcs.count(uniqueName) > 0) {
                        auto pNpcLocal = BroadcastNpcs[uniqueName];

                        if (pNpcLocal) {
                            pNpcLocal->destroyed = true;
                            BroadcastNpcs.erase(BroadcastNpcs.find(uniqueName));
                        }
                    }
                    if (SyncNpcs.count(uniqueName) > 0) {
                        auto remoteNpc = SyncNpcs[uniqueName];
                        if (remoteNpc) {
                            remoteNpc->destroyed = true;
                            SyncNpcs.erase(SyncNpcs.find(uniqueName));
                        }
                    }

                    UniqueNameToNpcList.erase(uniqueName);
                    NpcToUniqueNameList.erase(npc);
                }
            }
        }

        return Ivk_oCWorld_RemoveVob(_this, vob);
    }

    void __fastcall zCModel_StartAni(zCModel*, void*, zCModelAni*, int);
#if ENGINE >= Engine_G2
    CInvoke<void(__thiscall*)(zCModel*, zCModelAni*, int)> Ivk_zCModel_StartAni(0x0057B0C0, &zCModel_StartAni);
#else
    CInvoke<void(__thiscall*)(zCModel*, zCModelAni*, int)> Ivk_zCModel_StartAni(0x005612F0, &zCModel_StartAni);
#endif
    void __fastcall zCModel_StartAni(zCModel* _this, void* vtable, zCModelAni* a, int startMode) {

        /*
        // test NB sync anims
        if (_this->homeVob)
        {
            if (auto pNpc = _this->homeVob->CastTo<oCNpc>())
            {

                for each (auto n in BroadcastNpcs)
                {
                    if (n.second->npc == pNpc)
                    {
                        n.second->NB_AniId = a->aniID;
                        n.second->NB_AniMode = startMode;
                    }
                }

            }
        }

        Ivk_zCModel_StartAni(_this, a, startMode);

        return;
        */

        zSTRING info;

        if (_this->homeVob)
        {
            info += "vob: " + _this->homeVob->GetObjectName() + " type: " + Z _this->homeVob->type + " sl: " + Z _this->homeVob->sleepingMode + " w: " + Z(int)_this->homeVob->homeWorld;
        }
        else
        {
            info += "vob: NULL";
        }

        if (a)
        {
            info += " ani: " + a->aniName + " " + a->objectName;
        }
        else
        {
            info += " ani: NULL";
        }


        info += " mod: " + Z startMode;

        //cmd << info << endl;



        if (IsLoadingLevel) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        if (zinput->KeyPressed(KEY_F2)) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        if (IsCoopPaused) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }


        if (ClientThread && _this->homeVob) {

            if (auto pNpc = _this->homeVob->CastTo<oCNpc>())
            {
                //client's own real summons, their anims is free to use
                if (Npc_IsSummon(pNpc) && !remoteSummons.IsInList(pNpc))
                {
                    Ivk_zCModel_StartAni(_this, a, startMode);
                    return;
                }
                // remote summons
                else if (Npc_IsSummon(pNpc) && remoteSummons.IsInList(pNpc))
                {
                    if (startMode != COOP_MAGIC_NUMBER)
                    {
                        //cmd << "Block ani for " << pNpc->GetInstanceName() << endl;
                        return;
                    }
                    else
                    {
                        Ivk_zCModel_StartAni(_this, a, startMode);
                        //cmd << "Use ani for " << pNpc->GetInstanceName() << endl;
                        return;
                    }

                }
            }
            
        }
       

        if (ClientThread && startMode != COOP_MAGIC_NUMBER && _this->homeVob && IgnoredSyncNpc(_this->homeVob)) {
            Ivk_zCModel_StartAni(_this, a, 0);
            return;
        }

        if (ClientThread && startMode == COOP_MAGIC_NUMBER && _this->homeVob && IgnoredSyncNpc(_this->homeVob)) {
            return;
        }

        if (!ClientThread || _this->homeVob == player) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        if (ClientThread && startMode == COOP_MAGIC_NUMBER) {
            Ivk_zCModel_StartAni(_this, a, 0);
            return;
        }

        if (player && player->GetFocusNpc() == _this->homeVob && a && a->aniName && 
           (a->aniName == "S_RUN" || a->aniName == "S_WALK" || a->aniName == "T_LOOK" || a->aniName == "T_WALKTURNR" || a->aniName == "T_WALKTURNL")) 
        {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        if (ClientThread && _this->homeVob && _this->homeVob->GetCharacterClass() != zCVob::zTVobCharClass::zVOB_CHAR_CLASS_NPC) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        if (ClientThread && IsPlayerTalkingWithNpc(_this->homeVob)) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        if (ClientThread && !IsCoopPlayer(_this->GetObjectName()) && zCCSCamera::playing) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        auto npc = (oCNpc*)_this->homeVob;
        if (ClientThread && npc && npc->IsDead()) {
            Ivk_zCModel_StartAni(_this, a, startMode);
            return;
        }

        // TMP: Use the logic to start/stop anim only for NB, it works by just not calling the function in g1/g2
        if (!FriendInstanceId.Compare("ch") && a && a->aniName && (a->aniName == "S_RUN" || a->aniName == "S_FISTRUN")) {
            auto activeBefore = _this->IsAnimationActive(a->aniName);
            Ivk_zCModel_StartAni(_this, a, startMode);

            if (activeBefore == 0) {
                _this->StopAnimation(a->aniName);
            }
        }
    }


    HOOK ivk_oCNpc_DoDropVob_Union AS(&oCNpc::DoDropVob, &oCNpc::DoDropVob_Union);
    int oCNpc::DoDropVob_Union(zCVob* vob) {


       

        /*
        if (auto pItem = vob->CastTo<oCItem>())
        {
            NB_PrintWin("DropVob: " + this->GetInstanceName() + " fm: " + Z this->fmode + " hp: " + Z this->GetAttribute(NPC_ATR_HITPOINTS));
            NB_PrintWin("DropVobItem: " + pItem->GetInstanceName());
        }
        */

        if (this->IsAPlayer() && vob && (!this->IsDead() && !this->IsUnconscious() && this->fmode == NPC_WEAPON_NONE)) {

            if (auto pItem = vob->CastTo<oCItem>())
            {
                OnItemDrop(this, pItem);
            }
        }

        //fixme?
        if (this->GetInstanceName() == "PC_HEROMUL" && vob && (this->IsDead() || this->IsUnconscious() || this->GetAttribute(NPC_ATR_HITPOINTS) == 1))
        {
            if (auto pItem = vob->CastTo<oCItem>())
            {
                //NB_PrintWin("Protect: " + pItem->GetInstanceName());
            }
           
            this->ClearEM();
            return TRUE;
        }


        int result = THISCALL(ivk_oCNpc_DoDropVob_Union)(vob);

        return result;
    }

    HOOK ivk_oCNpc_DoTakeVob AS(&oCNpc::DoTakeVob, &oCNpc::DoTakeVob_Union);
    int oCNpc::DoTakeVob_Union(zCVob* vob) {
        if (this->IsAPlayer()) {

            if (oCItem* item = zDYNAMIC_CAST<oCItem>(vob)) {

                OnItemTake(this, item);
            }
        }

        return THISCALL(ivk_oCNpc_DoTakeVob)(vob);
    }
}
namespace GOTHIC_ENGINE {
    const int COOP_VERSION = 32;
    const int COOP_MAGIC_NUMBER = 7318;
    int BROADCAST_DISTANCE = 4500;

    DWORD MainThreadId;
    std::string PluginState = "";
    Chat* GameChat = new Chat();

    char GothicExeFolderPath[MAX_PATH];
    std::string GothicCoopLogPath;

    string MyselfId = "_player_";


    static LocalNpc* Myself = NULL;
    int LastFreePlayerId = 0;
    long long CurrentMs = 0;
    json CoopConfig;
    bool isClientConnected = false;
    bool wasClientDisconnect = false;
    int portSocket = 18991;
    static bool IsLoadingLevel = false;
    static bool IsSavingGame = false;
    static bool IsCoopPaused = false;
    long long LastUpdateListOfVisibleNpcs = 0;
    long long LastNpcListRefreshTime = 0;
    zVEC3* CurrentWorldTOTPosition;

    string FriendInstanceId = "ch";
    string MyNickname = "";

    float interpolateValue = 1.0f;
    int PlayersDamageMultipler = 100;
    int NpcsDamageMultipler = 100;
    int ToggleGameLogKey;
    int ToggleGameStatsKey;
    int StartServerKey;
    int StartConnectionKey;
    int ReinitPlayersKey;
    int RevivePlayerKey;
    Timer MainTimer;
    //last instance player for transforms
    zSTRING lastPlayerInst;

    int bytesSent = 0;
    int bytesRecieved = 0;
    int lastOnline = 1;
    float speedSent = 0;
    float speedRecieved = 0;


    static Thread* ServerThread = NULL;
    static Thread* ClientThread = NULL;

    std::map<string, LocalNpc*> BroadcastNpcs;
    std::map<string, RemoteNpc*> SyncNpcs;

    static std::map<string, oCNpc*> UniqueNameToNpcList;
    static std::map<oCNpc*, string> NpcToUniqueNameList;

    static std::map<string, int> NamesCounter;
    static std::map<oCNpc*, string> NpcToFirstRoutineWp;

    static std::map<oCNpc*, string> PlayerNpcs;
    static std::map<string, oCNpc*> PlayerNameToNpc;

    static std::map<string, oCNpc*> KilledByPlayerNpcNames;

    zCArray<oCNpc*> remoteSummons;


    static SafeQueue<json> ReadyToSendJsons;
    static SafeQueue<json> ReadyToBeDistributedPackets;
    static SafeQueue<ENetEvent> ReadyToBeReceivedPackets;
    static SafeQueue<PlayerHit> ReadyToSyncDamages;
    static SafeQueue<SpellCast> ReadyToSyncSpellCasts;

    static const int IgnoredSyncNpcsCount = 9;
    static const char IgnoredSyncNpcs[IgnoredSyncNpcsCount][30] =
    {
        {"AD_OLDGHOSTRIDDLE1AD_LAST"},
        {"AD_OLDGHOSTRIDDLE2AD_LAST"},
        {"AD_OLDGHOSTRIDDLE3AD_LAST"},
        {"AD_OLDGHOSTRIDDLE4AD_LAST"},
        {"AD_OLDGHOSTRIDDLE5AD_LAST"},
        {"AD_OLDGHOSTRIDDLE6AD_LAST"},
        {"AD_OLDGHOSTRIDDLE7AD_LAST"},
        {"AD_OLDGHOSTRIDDLE8AD_LAST"},
        {"WISP_DETECTOR"},
    };
}
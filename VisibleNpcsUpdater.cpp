namespace GOTHIC_ENGINE {
    void UpdateVisibleNpc() {
        PluginState = "UpdateVisibleNpc";

        if (ServerThread) {
            auto npcs = GetVisibleNpcs();
            std::map<string, LocalNpc*> updatedBroadcastNpcs;

            for each (auto npc in npcs)
            {
                if (NpcToUniqueNameList.count(npc) == 0) {
                    continue;
                }

                auto npcUniqueName = NpcToUniqueNameList[npc];
                if (BroadcastNpcs.count(npcUniqueName) == 0) {
                    auto newLocalNpc = new LocalNpc(npc, npcUniqueName);

                    //cmd << "New local npc: " << npc->GetInstanceName() << " uniq: " << npcUniqueName << endl;

                    updatedBroadcastNpcs[npcUniqueName] = newLocalNpc;
                }
                else {
                    updatedBroadcastNpcs[npcUniqueName] = BroadcastNpcs[npcUniqueName];
                }
            }

            BroadcastNpcs = updatedBroadcastNpcs;
        }

        
        if (ClientThread)
        {
            auto npcs = GetVisibleNpcs();
            std::map<string, LocalNpc*> updatedBroadcastNpcs;

            for each (auto npc in npcs)
            {
                if (NpcToUniqueNameList.count(npc) == 0) {
                    continue;
                }

                if (Npc_IsRemoteSummon(npc))
                {
                    continue;
                }

                if (!Npc_IsSummon(npc))
                {
                    continue;
                }

                auto npcUniqueName = NpcToUniqueNameList[npc];
                if (BroadcastNpcs.count(npcUniqueName) == 0) {
                    auto newLocalNpc = new LocalNpc(npc, npcUniqueName);

                    cmd << "New local npc CLIENT: " << npc->GetInstanceName() << " uniq: " << npcUniqueName << endl;

                    updatedBroadcastNpcs[npcUniqueName] = newLocalNpc;

                    newLocalNpc->Reinit();
                }
                else {
                    updatedBroadcastNpcs[npcUniqueName] = BroadcastNpcs[npcUniqueName];
                }
            }

            BroadcastNpcs = updatedBroadcastNpcs;
        }
        
    }
}
// Supported with union (c) 2020 Union team
// Union SOURCE file

namespace GOTHIC_ENGINE {
	
#ifdef NEWBALANCE_MOD

	// new balance dll interactions
	HMODULE module;


	typedef void(*takeStringFunc)(char*);
	typedef void(*voidFunc)();
	typedef void(*voidIntFunc)(int);
	typedef void(*callOnDead)(void*, void*);
	void InitExternalDll()
	{
		module = CPlugin::FindModule("UNION_ABI.DLL");
	}


	// Shows message on the screen and console
	void NB_PrintWin(char* mes)
	{
		auto func = (takeStringFunc)GetProcAddress(module, "Extern_ShowPrintWin");
		func(mes);
	}

	void NB_PrintWin(zSTRING mes)
	{
		auto func = (takeStringFunc)GetProcAddress(module, "Extern_ShowPrintWin");
		func(mes.ToChar());
	}

	// Sets in New Balance dll that multiplayer is active
	void NB_SetMultiplayerOn()
	{
		auto func = (voidIntFunc)GetProcAddress(module, "Extern_SetMultActive");
		func(TRUE);
	}

	void NB_BlockNextSummon()
	{
		auto func = (voidFunc)GetProcAddress(module, "Extern_BlockNextSummon");
		func();
	}

	void NB_CallOnDead(oCNpc* atk, oCNpc* target)
	{
		auto func = (callOnDead)GetProcAddress(module, "Extern_CallOnDead");
		func(atk, target);
	}

	extern SafeQueue<PlayerHit> ReadyToSyncDamages;

	extern "C"
	{
		__declspec(dllexport) void ExternMult_AddDamage(int damage, void* atk, void* target, int isDead, int isUnc, unsigned long damageMode) {

			cmd << "New damage: " << ((oCNpc*)atk)->GetInstanceName() << " " << damage << endl;

			PlayerHit hit;
			hit.damage = damage;
			hit.attacker = (oCNpc*)atk;
			hit.npc = (oCNpc*)target;
			hit.isDead = isDead;
			hit.isUnconscious = isUnc;
			hit.damageMode = damageMode;
			ReadyToSyncDamages.enqueue(hit);
		}
	}
#endif // NEWBALANCE_MOD
}
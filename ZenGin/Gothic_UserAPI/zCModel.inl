// Supported with union (c) 2020 Union team

// User API for zCModel
// Add your methods here

zCModelAni* GetAniFromAniName(const zSTRING& aniName) const;
int IsAniActive(zSTRING anim);
zBOOL zCModel::IsAniActiveEngine(int aniID);
zBOOL zCModel::IsAniActiveEngine(zSTRING& aniName);
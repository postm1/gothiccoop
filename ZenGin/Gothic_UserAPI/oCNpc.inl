// Supported with union (c) 2020 Union team

// User API for oCNpc
// Add your methods here

void ApplyOverlayMds(const zSTRING& mds);
void RemoveOverlayMds(const zSTRING& mds);
void oCNpc::ApplyOverlaysNpc(oCNpc* npc);
void oCNpc::ApplyOverlaysArray(zCArray<int> npcMdsArray);
int oCNpc::CompareOverlaysMds(oCNpc* npc);
int oCNpc::CompareOverlaysArray(zCArray<int> npcMdsList);
oCItem* oCNpc::GetEquippedHelmet(void);
bool oCNpc::FMode_IsFar();
bool oCNpc::FMode_IsMelee();
int oCNpc::DoDropVob_Union(zCVob* vob);
int oCNpc::DoTakeVob_Union(zCVob* vob);
void oCNpc::DropItemByIndex(int index, int itemFlag, int count, zSTRING uniqName);
void oCNpc::OnDamage_Hit_Union(oSDamageDescriptor& dam);
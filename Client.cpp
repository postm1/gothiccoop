namespace GOTHIC_ENGINE {
    int CoopClientThread()
    {
        if (enet_initialize() != 0)
        {
            ChatLog("An error occurred while initializing ENet.");
            ClientThread = NULL;
            return EXIT_FAILURE;
        }
        atexit(enet_deinitialize);

        ENetHost* client;
        client = enet_host_create(NULL, 1, 2, 0, 0);
        if (client == NULL)
        {
            ChatLog("An error occurred while trying to create an ENet client host.");
            ClientThread = NULL;
            return EXIT_FAILURE;
        }

        ENetAddress address;
        ENetEvent event;
        ENetPeer* peer;
        auto serverIp = CoopConfig["server"].get<std::string>();
        enet_address_set_host(&address, serverIp.c_str());
        address.port = portSocket;
        peer = enet_host_connect(client, &address, 2, 0);
        if (peer == NULL)
        {
            ChatLog("No available peers for initiating an ENet connection.");
            ClientThread = NULL;
            return EXIT_FAILURE;
        }

        if (enet_host_service(client, &event, 5000) > 0 &&
            event.type == ENET_EVENT_TYPE_CONNECT)
        {
            ChatLog(string::Combine("Connection to the server %s OK (v. %i).", string(serverIp.c_str()), COOP_VERSION), GFX_GREEN);
            isClientConnected = true;
            wasClientDisconnect = false;
        }
        else
        {
            enet_peer_reset(peer);
            ChatLog(string::Combine("Connection to the server %s FAIL (v. %i).", string(serverIp.c_str()), COOP_VERSION), GFX_RED);
            ClientThread = NULL;
            return EXIT_FAILURE;
        }

        while (true) {
            ENetEvent event;
            auto eventStatus = enet_host_service(client, &event, 1);

            if (eventStatus > 0) {
                ReadyToBeReceivedPackets.enqueue(event);
            }

            if (!ReadyToSendJsons.isEmpty()) {

                auto j = ReadyToSendJsons.dequeue();

                auto updateJSON = j.dump();

                auto type = j["type"].get<int>();

                if (IsUnreliable((UpdateType)type))
                {
                    ENetPacket* packet = enet_packet_create(updateJSON.c_str(), strlen(updateJSON.c_str()) + 1, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT);
                    enet_peer_send(peer, 0, packet);
                }
                else
                {
                    ENetPacket* packet = enet_packet_create(updateJSON.c_str(), strlen(updateJSON.c_str()) + 1, ENET_PACKET_FLAG_RELIABLE);
                    enet_peer_send(peer, 0, packet);
                }

            }
        }
    }
}
namespace GOTHIC_ENGINE {
	RemoteNpc* addSyncedNpc(string uniqueName) {
		auto newSyncNpc = new RemoteNpc(uniqueName);
		SyncNpcs[uniqueName] = newSyncNpc;
		return newSyncNpc;
	}

	void removeSyncedNpc(string uniqueName) {
		auto removedNpc = SyncNpcs[uniqueName];
		json j; 
		j["type"] = DESTROY_NPC;
		
		if (removedNpc)
		{
			removedNpc->localUpdates.push_back(j);
		}
		
	}

	void OnItemDrop(oCNpc* npc, oCItem* pItem) {


		if (npc && Myself && pItem)
		{
			int randVal = GetRandVal(0, 2e9);

			//cmd << "Item DROP: " << pItem->GetInstanceName() << endl;
			pItem->SetObjectName("RX_DROPPED_ITEM_" + Z randVal);
			Myself->pItemDropped = pItem;
			Myself->itemDropReady = true;
			Myself->SyncOnDropItem();

			if (pItem->HasFlag(ITM_FLAG_ACTIVE) || pItem->IsArmor() || pItem->IsHelmet())
			{

				// forcely update armor & helmet
				Myself->lastArmorName = zSTRING("");
				Myself->lastHelmetName = zSTRING("");
			}
		}

	}

	void OnItemTake(oCNpc* npc, oCItem* pItem) {


		if (npc && Myself && pItem)
		{

			if (pItem->GetObjectName().HasWord("RX_DROPPED_ITEM_"))
			{
				//cmd << "Item take FROM: " << pItem->GetInstanceName() << " " << pItem->GetObjectName() << endl;

				
				Myself->pItemTaken = CreateCoopItem(pItem->GetInstance());
				Myself->pItemTaken->flags = pItem->flags;
				Myself->pItemTaken->amount = pItem->amount;
				Myself->pItemTaken->SetObjectName(pItem->GetObjectName());
				Myself->pItemTakenPos = pItem->GetPositionWorld();

				Myself->SyncOnTakeItem();
			}
		}

	}
}
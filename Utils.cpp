// Supported with union (c) 2020 Union team
// Union SOURCE file

namespace GOTHIC_ENGINE {
	
	class LocalNpc;

	const char* NPC_NODE_TORCH_NB = "BIP01 L HAND"; // torch
	const char* NPC_NODE_TROPHY_NB = "BIP01 PELVIS"; //trophy like dragon skull
	const char* NPC_NODE_THIGH_NB = "BIP01 L THIGH"; //lighter

	const int GIL_NONE = 0;
	const int GIL_HUMAN = 1;
	const int GIL_PAL = 1;
	const int GIL_MIL = 2;
	const int GIL_VLK = 3;
	const int GIL_KDF = 4;
	const int GIL_NOV = 5;
	const int GIL_DJG = 6;
	const int GIL_SLD = 7;
	const int GIL_BAU = 8;
	const int GIL_BDT = 9;
	const int GIL_STRF = 10;
	const int GIL_OUT = 10;
	const int GIL_DMT = 11;
	const int GIL_SEK = 12;
	const int GIL_PIR = 13;
	const int GIL_KDW = 14;
	const int GIL_GUR = 15;
	const int GIL_TPL = 16;
	const int GIL_NDW = 17;
	const int GIL_NDM = 18;
	const int GIL_KDM = 19;
	const int GIL_PUBLIC = 20;
	const int GIL_SEPERATOR_HUM = 21;
	const int GIL_MEATBUG = 22;
	const int GIL_SHEEP = 23;
	const int GIL_GOBBO = 24;
	const int GIL_GOBBO_SKELETON = 25;
	const int GIL_SUMMONED_GOBBO_SKELETON = 26;
	const int GIL_SCAVENGER = 27;
	const int GIL_Giant_Rat = 28;
	const int GIL_GIANT_BUG = 29;
	const int GIL_BLOODFLY = 30;
	const int GIL_WARAN = 31;
	const int GIL_WOLF = 32;
	const int GIL_SUMMONED_WOLF = 33;
	const int GIL_MINECRAWLER = 34;
	const int GIL_LURKER = 35;
	const int GIL_SKELETON = 36;
	const int GIL_SUMMONED_SKELETON = 37;
	const int GIL_SKELETON_MAGE = 38;
	const int GIL_ZOMBIE = 39;
	const int GIL_SNAPPER = 40;
	const int GIL_SHADOWBEAST = 41;
	const int GIL_SHADOWBEAST_SKELETON = 42;
	const int GIL_HARPY = 43;
	const int GIL_STONEGOLEM = 44;
	const int GIL_FIREGOLEM = 45;
	const int GIL_ICEGOLEM = 46;
	const int GIL_SUMMONED_GOLEM = 47;
	const int GIL_DEMON = 48;
	const int GIL_SUMMONED_DEMON = 49;
	const int GIL_TROLL = 50;
	const int GIL_SWAMPSHARK = 51;
	const int GIL_DRAGON = 52;
	const int GIL_MOLERAT = 53;
	const int GIL_Alligator = 54;
	const int GIL_SWAMPGOLEM = 55;
	const int GIL_Stoneguardian = 56;
	const int GIL_Gargoyle = 57;
	const int GIL_BLOODFLY_SUMMONED = 58;
	const int GIL_SummonedGuardian = 59;
	const int GIL_SummonedZombie = 60;
	const int GIL_SEPERATOR_ORC = 61;
	const int GIL_ORC = 62;
	const int GIL_FRIENDLY_ORC = 63;
	const int GIL_UNDEADORC = 64;
	const int GIL_DRACONIAN = 65;
	const int GIL_MAX = 66;

	const int SPL_PalLight = 0;
	const int SPL_PalLightHeal = 1;
	const int SPL_PalHolyBolt = 2;
	const int SPL_PalMediumHeal = 3;
	const int SPL_PalRepelEvil = 4;
	const int SPL_PalFullHeal = 5;
	const int SPL_PalDestroyEvil = 6;
	const int SPL_PalTeleportSecret = 7;
	const int SPL_TeleportSeaport = 8;
	const int SPL_TeleportMonastery = 9;
	const int SPL_TeleportFarm = 10;
	const int SPL_TeleportXardas = 11;
	const int SPL_TeleportPassNW = 12;
	const int SPL_TeleportPassOW = 13;
	const int SPL_TeleportOC = 14;
	const int SPL_TeleportOWDemonTower = 15;
	const int SPL_TeleportTaverne = 16;
	const int SPL_TPLLIGHTHEAL = 17;
	const int SPL_Light = 18;
	const int SPL_Firebolt = 19;
	const int SPL_Icebolt = 20;
	const int SPL_LightHeal = 21;
	const int SPL_SummonGoblinSkeleton = 22;
	const int SPL_InstantFireball = 23;
	const int SPL_Zap = 24;
	const int SPL_SummonWolf = 25;

	const int SPL_WindFist = 26;
	const int SPL_Sleep = 27;
	const int SPL_MediumHeal = 28;
	const int SPL_LightningFlash = 29;
	const int SPL_ChargeFireball = 30;
	const int SPL_SummonSkeleton = 31;
	const int SPL_Fear = 32;
	const int SPL_IceCube = 33;
	const int SPL_ChargeZap = 34;
	const int SPL_SummonGolem = 35;
	const int SPL_DestroyUndead = 36;
	const int SPL_Pyrokinesis = 37;
	const int SPL_Firestorm = 38;
	const int SPL_IceWave = 39;
	const int SPL_SummonDemon = 40;
	const int SPL_FullHeal = 41;
	const int SPL_Firerain = 42;
	const int SPL_BreathOfDeath = 43;
	const int SPL_MassDeath = 44;
	const int SPL_ArmyOfDarkness = 45;
	const int SPL_Shrink = 46;
	const int SPL_TRFSHEEP_NB = 47;
	const int SPL_TrfScavenger = 48;
	const int SPL_TRFMEATBUG = 49;
	const int SPL_TrfGiantBug = 50;
	const int SPL_TrfWolf = 51;
	const int SPL_TrfWaran = 52;
	const int SPL_TrfSnapper = 53;
	const int SPL_TrfWarg = 54;
	const int SPL_TrfFireWaran = 55;
	const int SPL_TrfLurker = 56;
	const int SPL_TrfShadowbeast = 57;
	const int SPL_TRFDRAGONSNAPPER_NB = 58;
	const int SPL_Charm = 59;
	const int SPL_MasterOfDisaster = 60;
	const int SPL_Deathbolt = 61;
	const int SPL_Deathball = 62;
	const int SPL_ConcussionBolt = 63;
	const int SPL_TELEPORTORC = 64;
	const int SPL_BELIARRUNE = 65;
	const int SPL_TELEPORTPSICAMP = 66;
	const int SPL_TPLMEDIUMHEAL = 67;
	const int SPL_TPLHEAVYHEAL = 68;
	const int SPL_TPLSUPERHEAL = 69;
	const int SPL_Thunderstorm = 70;
	const int SPL_Whirlwind = 71;
	const int SPL_WaterFist = 72;
	const int SPL_IceLance = 73;
	const int SPL_Inflate = 74;
	const int SPL_Geyser = 75;
	const int SPL_Waterwall = 76;
	const int SPL_TPLLIGHTSTRIKE = 77;
	const int SPL_TPLMEDIUMSTRIKE = 78;
	const int SPL_TPLHEAVYSTRIKE = 79;
	const int SPL_SUMMONFIREGOLEM = 80;
	const int SPL_Swarm = 81;
	const int SPL_GreenTentacle = 82;
	const int SPL_TELEKINESIS = 83;
	const int SPL_SummonGuardian = 84;
	const int SPL_EnergyBall = 85;
	const int SPL_ManaForLife = 86;
	const int SPL_Skull = 87;
	const int SPL_SummonZombie = 88;
	const int SPL_SUMMONICEGOLEM = 89;
	const int SPL_TELEPORTDAGOT = 90;
	const int SPL_SUMMONELIGOR = 91;
	const int SPL_TPLSUPERSTRIKE = 92;
	const int SPL_FIRELIGHT = 93;
	const int SPL_CONTROL = 94;
	const int SPL_BERZERK = 95;
	const int SPL_SEVEREFETIDITY = 96;
	const int SPL_SummonCrait = 97;
	const int SPL_SUMMONSHOAL = 98;
	const int SPL_UnlockChest = 99;
	const int SPL_SUMMONSWAMPGOLEM = 100;
	const int SPL_DESTROYGUARDIANS = 101;
	const int SPL_SUMMONTREANT = 102;
	const int SPL_FireMeteor = 103;
	const int SPL_GuruWrath = 104;
	const int SPL_OrcFireball = 105;
	const int SPL_TrfTroll = 106;
	const int SPL_RapidFirebolt = 107;
	const int SPL_RapidIcebolt = 108;
	const int SPL_Rage = 109;
	const int SPL_Quake = 110;
	const int SPL_MagicCage = 111;
	const int SPL_Lacerate = 112;
	const int SPL_Extricate = 113;
	const int SPL_Explosion = 114;
	const int SPL_Elevate = 115;
	const int SPL_AdanosBall = 116;
	const int SPL_Acid = 117;


	const int SPL_LIGHTNINGSPHERE = 118;
	const int SPL_ELECTROWAVE = 119;
	const int SPL_FIREWAVE = 120;
	const int SPL_BLOODRAIN = 121;
	const int SPL_ICEEXPLOISION = 122;
	const int SPL_ICERAIN = 123;
	const int SPL_ABYSSSPELL = 124;

	const int SPL_MAGSPHERE = 125;
	const int SPL_STONEFIRST = 126;

	const int SPL_GREENSTRIKE = 127;
	const int SPL_FIRELANCE = 128;
	const int SPL_DEATHSTRIKE = 129;
	const int SPL_NECROBOLT = 130;
	const int SPL_GURUMASS = 131;
	const int SPL_CRESTOFELEMENTS = 132;
	const int SPL_SUMKHUB = 134;

	const int SPL_AGRO = 135;

	const int SPL_DOT_FIRST = 136;
	const int SPL_DARK_INVIS = 137;
	const int SPL_DARKCONTROL = 138;
	const int SPL_DARKSLEEP = 139;
	const int SPL_MASSAGRO = 140;
	const int SPL_MASSDOT = 141;
	const int SPL_DOT_SECOND = 142;
	const int SPL_DARKBALL = 143;
	const int SPL_DARKSPEAR = 144;
	const int SPL_BIGDARKBALL = 145;
	const int SPL_DARKWAVE = 146;
	const int SPL_TORTURE = 147;
	const int SPL_SUMDSNAPPER = 148;
	const int SPL_DARKPETSUMMON = 149;
	const int SPL_DARKRUNE_STEAL = 150;
	const int SPL_DARKRUNE_DEFENSE = 151;
	const int SPL_DARKRUNE_ILLNESS = 152;
	const int SPL_DARKRUNE_HEALPET = 153;
	const int SPL_DARKRUNE_Absorb = 154;
	const int SPL_DARKRUNE_LORD = 155;
	const int SPL_DARKSTATSUP = 156;
	const int SPL_DARKRUNE_EDEFENSE = 157;
	const int SPL_DRUIDRUNEQUEST = 158;
	const int SPL_SUMMONSKELETON_ARCHER = 159;
	const int SPL_SUMMONSKELETON_SPEAR = 160;

	const int SPL_SUMJINA = 161;
	const int SPL_SUMMON_REALGOBLIN = 162;
	const int SPL_AURA_SUMMON = 163;
	const int SPL_SUMMON_SKELETON_UNIQ = 164;
	const int SPL_SUMMON_SKELETON_SHADOWBEAST = 165;

	const int MAX_SPELL = 166;


#define ITEM_DAG 65539
#define ITEM_THROW 262147
#define FACTOR 81.919998
#define F(a) a * FACTOR			// 8


    // interpolation for translation
    zVEC3 Lerp(zVEC3 a, zVEC3 b, float t)
    {
        return a + (b - a) * t;
    }

    // min max limit for a float variable
    void zClamp(float& value, float bl, float bh) {
        if (value < bl)
            value = bl;
        else if (value > bh)
            value = bh;
    }

	bool CheckVarsForNAN(float x, float y, float z)
	{
		if (isnan(x) || isnan(y) || isnan(z))
		{
			return true;
		}

		return false;
	}

	static oCNpc* oldSelfNpc = NULL;
	static oCNpc* oldOtherNpc = NULL;
	static oCNpc* oldVictimNpc = NULL;


	// some function using when spawn/delete NPC
	void SaveParserVars() {
		oldSelfNpc = NULL;
		oldOtherNpc = NULL;
		oldVictimNpc = NULL;

		// Save self & other
		zCPar_Symbol* sym = parser->GetSymbol("SELF");
		if (sym) oldSelfNpc = dynamic_cast<oCNpc*>((zCVob*)sym->GetInstanceAdr());

		sym = parser->GetSymbol("OTHER");
		if (sym) oldOtherNpc = dynamic_cast<oCNpc*>((zCVob*)sym->GetInstanceAdr());

		sym = parser->GetSymbol("VICTIM");
		if (sym) oldVictimNpc = dynamic_cast<oCNpc*>((zCVob*)sym->GetInstanceAdr());
	}


	void RestoreParserVars() {
		parser->SetInstance("SELF", oldSelfNpc);
		parser->SetInstance("OTHER", oldOtherNpc);
		parser->SetInstance("VICTIM", oldVictimNpc);
	}

	Common::Map<zSTRING, int> MdsHumansMap;

	void CheckMdsMap() {
		static int check = 0;
		if (!check) {
			check = 1;
			MdsHumansMap.Insert("HUMANS.MDS", 0);
			MdsHumansMap.Insert("HUMANS_RELAXED.MDS", 1);
			MdsHumansMap.Insert("HUMANS_1HST2.MDS", 2);
			MdsHumansMap.Insert("HUMANS_2HST2.MDS", 3);
			MdsHumansMap.Insert("HUMANS_BOWT2.MDS", 4);
			MdsHumansMap.Insert("HUMANS_CBOWT2.MDS", 5);
			MdsHumansMap.Insert("HUMANS_MILITIA.MDS", 6);
			MdsHumansMap.Insert("HUMANS_1HST3.MDS", 7);
			MdsHumansMap.Insert("HUMANS_2HST3.MDS", 8);
			MdsHumansMap.Insert("HUMANS_ARROGANCE.MDS", 9);
			MdsHumansMap.Insert("HUMANS_1HST1.MDS", 10);
			MdsHumansMap.Insert("HUMANS_2HST1.MDS", 11);
			MdsHumansMap.Insert("HUMANS_BOWT1.MDS", 12);
			MdsHumansMap.Insert("HUMANS_CBOWT1.MDS", 13);
			MdsHumansMap.Insert("HUMANS_SPST2.MDS", 14);
			MdsHumansMap.Insert("HUMANS_ACROBATIC.MDS", 15);
			MdsHumansMap.Insert("HUMANS_MAGESPRINT.MDS", 16);
			MdsHumansMap.Insert("HUMANS_TIRED.MDS", 17);
			MdsHumansMap.Insert("HUMANS_MAGE.MDS", 18);
			MdsHumansMap.Insert("HUMANS_SKELETON.MDS", 19);
			MdsHumansMap.Insert("HUMANS_SKELETON_FLY.MDS", 20);
			MdsHumansMap.Insert("HUMANS_BABE.MDS", 21);
			MdsHumansMap.Insert("SHIELD.MDS", 22);
			MdsHumansMap.Insert("HUMANS_PIRATE.MDS", 23);
			MdsHumansMap.Insert("HUMANS_ARR.MDS", 24);
			MdsHumansMap.Insert("SHIELD_ST1.MDS", 25);
			MdsHumansMap.Insert("HUMANS_SPST1.MDS", 26);
			MdsHumansMap.Insert("HUMANS_TRD.MDS", 27);
			MdsHumansMap.Insert("HUMANS_SIT_EAT.MDS", 28);
			MdsHumansMap.Insert("HUMANS_SIT_DRINK.MDS", 29);
			MdsHumansMap.Insert("SHIELD_ST2.MDS", 30);
			MdsHumansMap.Insert("HUMANS_REL.MDS", 31);
			MdsHumansMap.Insert("HUMANS_AXEST2.MDS", 32);
			MdsHumansMap.Insert("HUMANS_NEWTORCH.MDS", 33);

		}
	}

	int GetMdsIndex(zSTRING mds) {
		CheckMdsMap();
		mds.Upper();

		auto arr = MdsHumansMap.GetArray();
		for (auto i = 0; i < arr.GetNum(); i++) {
			if (auto pair = arr.GetSafe(i)) {
				if (mds == pair->GetKey()) {
					return pair->GetValue();
				}
			}
		}


		//cmd << "GetMdsIndex invalid " << mds << endl;

		return -1;
	}

	zSTRING GetMdsByIndex(int index) {
		CheckMdsMap();
		auto arr = MdsHumansMap.GetArray();
		for (int i = 0; i < arr.GetNum(); i++) {
			if (auto pair = arr.GetSafe(i)) {
				if (index == pair->GetValue()) {
					return pair->GetKey();
				}
			}
		}

		return "";
	}

	zCArray<int> GetNpcMds(oCNpc* npc) {
		zCArray<int> arr;
		if (npc && npc->GetModel()) {
			auto proto = npc->GetModel()->modelProtoList;
			for (int i = 0; i < proto.GetNumInList(); i++) {
				if (auto protoInst = proto.GetSafe(i)) {
					arr.Insert(GetMdsIndex(protoInst->modelProtoFileName));
				}
			}
		}
		return arr;
	}

	void oCNpc::ApplyOverlaysNpc(oCNpc* npc) {
		if (!npc) return;

		zCArray<int> npcMdsList = GetNpcMds(npc);
		ApplyOverlaysArray(npcMdsList);
	}

	void oCNpc::ApplyOverlaysArray(zCArray<int> npcMdsArray) {
		zCArray<zCModelPrototype*> protoList = GetModel()->modelProtoList;
		zCArray<zSTRING> mdsList;
		for (int i = 0; i < protoList.GetNumInList(); i++) {
			if (zCModelPrototype* proto = protoList.GetSafe(i)) {
				if (proto->modelProtoFileName != "HUMANS.MDS") {
					mdsList.Insert(proto->modelProtoFileName);
				}
			}
		}

		for (int i = 0; i < mdsList.GetNumInList(); i++) {
			zSTRING mds = mdsList.GetSafe(i);
			this->RemoveOverlayMds(mds);
		}

		for (int i = 0; i < npcMdsArray.GetNumInList(); i++) {
			int index = npcMdsArray.GetSafe(i);
			zSTRING mds = GetMdsByIndex(index);

			if (!mds.IsEmpty() && mds != "HUMANS.MDS") {
				this->ApplyOverlayMds(mds);
			}
		}
	}

	int oCNpc::CompareOverlaysMds(oCNpc* npc) {
		if (!npc) return 0;
		zCArray<int> npcMdsList = GetNpcMds(npc);
		zCArray<int> npcMdsListMy = GetNpcMds(this);
		if (npcMdsListMy.GetNumInList() != npcMdsList.GetNumInList()) {
			return 0;
		}
		else {
			for (int i = 0; i < npcMdsListMy.GetNumInList(); i++) {
				int mdsMy = npcMdsListMy.GetSafe(i);
				if (!npcMdsList.IsInList(mdsMy)) {
					return 0;
				}
			}
		}


		return 1;
	}

	int oCNpc::CompareOverlaysArray(zCArray<int> npcMdsList) {

		zCArray<int> npcMdsListMy = GetNpcMds(this);

		if (npcMdsListMy.GetNumInList() != npcMdsList.GetNumInList()) {
			return 0;
		}
		else {
			for (int i = 0; i < npcMdsListMy.GetNumInList(); i++) {
				int mdsMy = npcMdsListMy.GetSafe(i);
				if (!npcMdsList.IsInList(mdsMy)) {
					return 0;
				}
			}
		}


return 1;
	}


	void oCNpc::RemoveOverlayMds(const zSTRING& mds) {
		RemoveOverlay(mds);
		if (GetAnictrl())
			GetAnictrl()->InitAnimations();
	};

	void oCNpc::ApplyOverlayMds(const zSTRING& mds) {
		ApplyOverlay(mds);
		if (GetAnictrl())
			GetAnictrl()->InitAnimations();
	};

	// Get current npc helmet
	oCItem* oCNpc::GetEquippedHelmet(void) {
		if (!this)
			return NULL;

		oCItem* helmet = NULL;
		oCNpcInventory* inv = &this->inventory2;
		if (inv) {
			zCListSort<oCItem>* pInv = inv->inventory.next;
			int ITM_CAT_ARMOR = 16;
			int ITM_WEAR_HEAD = 2;
			int ITM_FLAG_ACTIVE = 1 << 30;
			while (pInv) {
				oCItem* item = pInv->data;
				if (item) {
					if (item->mainflag == ITM_CAT_ARMOR
						&& item->wear == ITM_WEAR_HEAD
						&& item->HasFlag(ITM_FLAG_ACTIVE)) {
						helmet = item;
						break;
					}
				}
				pInv = pInv->next;
			}
		}

		return helmet;
	}


	bool oCNpc::FMode_IsFar() {
		return (this->fmode == NPC_WEAPON_BOW || this->fmode == NPC_WEAPON_CBOW);
	}


	bool oCNpc::FMode_IsMelee() {
		return (this->fmode >= NPC_WEAPON_DAG && this->fmode <= NPC_WEAPON_2HS);
	}

	// set int variable in scripts
	void zCParser::SetInt(zSTRING name, int value) {
		zCPar_Symbol* sym = GetSymbol(name);

		if (sym) {
			sym->single_intdata = value;
		}
	}


	// call void script function by its name
	void zCParser::CallFuncByName(zSTRING name) {
		int idx = GetIndex(name);

		if (idx != -1) {
			CallFunc(idx);
		}
	}




	void RX_CatchErr(zSTRING err)
	{
		cmd << err << endl;
		Message::Box(err);
	}


	void RX_Assert(bool expr, zSTRING err)
	{
		if (!expr)
		{
			RX_CatchErr(err);
		}
	}

	Common::string GetNpcUniqName(oCNpc* npc)
	{
		Common::string result = "";

		if (npc && NpcToUniqueNameList.count(npc) > 0)
		{
			result = NpcToUniqueNameList[npc];
		}


		return result;
	}

	


	zSTRING GetPacketTypeAsString(UpdateType type)
	{
		zSTRING name;

		switch (type)
		{
		case SYNC_POS: name = "SYNC_POS"; break;
		case SYNC_HEADING: name = "SYNC_HEADING"; break;
		case SYNC_ANIMATION: name = "SYNC_ANIMATION"; break;
		case SYNC_WEAPON_MODE: name = "SYNC_WEAPON_MODE"; break;
		case INIT_NPC: name = "INIT_NPC"; break;
		case DESTROY_NPC: name = "DESTROY_NPC"; break;
		case SYNC_ATTACKS: name = "SYNC_ATTACKS"; break;
		case SYNC_ARMOR: name = "SYNC_ARMOR"; break;
		case SYNC_WEAPONS: name = "SYNC_WEAPONS"; break;
		case SYNC_HP: name = "SYNC_HP"; break;
		case SYNC_TIME: name = "SYNC_TIME"; break;
		case SYNC_HAND: name = "SYNC_HAND"; break;
		case SYNC_MAGIC_SETUP: name = "SYNC_MAGIC_SETUP"; break;
		case SYNC_SPELL_CAST: name = "SYNC_SPELL_CAST"; break;
		case SYNC_REVIVED: name = "SYNC_REVIVED"; break;
		case SYNC_PROTECTIONS: name = "SYNC_PROTECTIONS"; break;
		case SYNC_PLAYER_NAME: name = "SYNC_PLAYER_NAME"; break;
		case PLAYER_DISCONNECT: name = "PLAYER_DISCONNECT"; break;
		case SYNC_TALENTS: name = "SYNC_TALENTS"; break;
		case SYNC_BODYSTATE: name = "SYNC_BODYSTATE"; break;
		case SYNC_OVERLAYS: name = "SYNC_OVERLAYS"; break;
		case SYNC_HELMET: name = "SYNC_HELMET"; break;
		case SYNC_SHIELD_SLOT: name = "SYNC_SHIELD_SLOT"; break;
		case SYNC_TORCH_SLOT: name = "SYNC_TORCH_SLOT"; break;
		case SYNC_LEFT_HAND_SLOT: name = "SYNC_LEFT_HAND_SLOT"; break;
		case SYNC_RIGHT_HAND_SLOT: name = "SYNC_RIGHT_HAND_SLOT"; break;
		case SYNC_TROPHY_SLOT: name = "SYNC_TROPHY_SLOT"; break;
		case SYNC_DROPITEM: name = "SYNC_DROPITEM"; break;
		case SYNC_TAKEITEM: name = "SYNC_TAKEITEM"; break;
		case SYNC_THIGH: name = "SYNC_THIGH"; break;
		case SYNC_ANIM_NB: name = "SYNC_ANIM_NB"; break;
		case SYNC_CHAGEGUILD: name = "SYNC_CHAGEGUILD"; break;
			
		default: name = "UNKNOWN"; break;
		}

		return name;
	}

	// gets if a packet is rare, not every frame
	bool IsUniqPacket(UpdateType type)
	{
		return type != SYNC_POS && type != SYNC_HEADING && type != SYNC_HP && type != SYNC_ANIMATION && type != SYNC_BODYSTATE;
	}

	bool IsUnreliable(UpdateType type)
	{
		return type == SYNC_POS || type == SYNC_HEADING;
	}

	int IsSummonRune(int id) {
		if (id == SPL_SummonGoblinSkeleton
			|| id == SPL_ArmyOfDarkness
			|| id == SPL_SummonSkeleton
			|| id == SPL_SummonDemon
			|| id == SPL_SummonZombie

			|| id == SPL_SUMMONFIREGOLEM
			|| id == SPL_SUMMONICEGOLEM
			|| id == SPL_SUMMONSWAMPGOLEM
			|| id == SPL_SummonGolem

			|| id == SPL_SummonCrait

			|| id == SPL_SummonGuardian

			|| id == SPL_SUMMONSHOAL
			|| id == SPL_SummonWolf
			|| id == SPL_SUMKHUB
			|| id == SPL_DARKPETSUMMON
			|| id == SPL_SUMMONSKELETON_ARCHER
			|| id == SPL_SUMMONSKELETON_SPEAR
			|| id == SPL_SUMJINA
			|| id == SPL_SUMMON_REALGOBLIN
			|| id == SPL_SUMMON_SKELETON_UNIQ
			|| id == SPL_SUMMON_SKELETON_SHADOWBEAST
			) {
			return TRUE;
		}
		return FALSE;
	}

	void oCNpc::DropItemByIndex(int index, int itemFlag, int count, zSTRING uniqName)
	{
		oCItem* item;

		if (index == -1) return;
		if (count <= 0) return;

		//cmd << "Npc::DropItemByIndex: first check ok: Count: " << count << endl;

		item = this->IsInInv(index, count);

		if (!item && (itemFlag & ITM_FLAG_ACTIVE) == ITM_FLAG_ACTIVE)
		{
			//cmd << "EQUIPED ITEM NOT FOUND" << item->GetInstanceName() << endl;
			return;
		}

		if (!item)
		{
			//cmd << "craete new item!" << endl;

			item = CreateCoopItem(index);
			item->amount = count;
		}
		else
		{
			//cmd << "Found item!" << endl;
		}

		if (item)
		{
			//item->flags = itemFlag;


			//cmd << "Npc::DropItemByIndex: " << item->GetInstanceName() << " AMount: " << item->amount << " hw: " << (int)item->homeWorld << " UniqName: " << uniqName << endl;

			//oCItem* what = pNpc->RemoveFromInv(item, count);
			if (item)
			{
				if (item->HasFlag(ITM_FLAG_ACTIVE))
				{
					this->UnequipItem(item);
				}
				else
				{
					//cmd << "No active flag!" << endl;
				}
				
				item->SetObjectName(uniqName);

				this->DoDropVob(item);

			}
		}

	}

	int GetRandVal(int min, int max) {
		return (rand() % (max - min + 1) + min);
	}

	zCArray<zCVob*> CollectVobsInRadius(zVEC3 pos, int radius)
	{
		zTBBox3D box;
		zCArray<zCVob*> vobList;

		if (!player->GetHomeWorld())
		{
			return vobList;
		}

		box.maxs = pos + (zVEC3(1, 1, 1) * radius);
		box.mins = pos - (zVEC3(1, 1, 1) * radius);

		oCNpc* other = NULL;


		player->GetHomeWorld()->bspTree.bspRoot->CollectVobsInBBox3D(vobList, box);

		return vobList;
	}

	bool oCItem::IsArmor() {
		if (!this || this->mainflag != ITM_CAT_ARMOR)
			return FALSE;

		return this->wear == ITM_WEAR_TORSO ? 1 : 0;
	};


	bool oCItem::IsHelmet() {
		if (!this || this->mainflag != ITM_CAT_ARMOR)
			return FALSE;

		return this->wear == ITM_WEAR_HEAD ? 1 : 0;

	};


	bool oCItem::IsShield() {
		if (!this || this->mainflag != ITM_CAT_NF)
			return FALSE;

		return this->HasFlag(ITM_FLAG_SHIELD) && !this->HasFlag(ITEM_THROW);
	};

	zCModelAni* zCModel::GetAniFromAniName(const zSTRING& aniName) const {
		return GetAniFromAniID(GetAniIDFromAniName(aniName));
	};

	int zCModel::IsAniActive(zSTRING anim) {
		return IsAniActive(GetAniFromAniName(anim));
	};

	zBOOL zCModel::IsAniActiveEngine(int aniID)
	{
		return IsAniActive(GetAniFromAniID(aniID));
	}

	zBOOL zCModel::IsAniActiveEngine(zSTRING& aniName) {
		return IsAniActive(GetAniFromAniName(aniName));
	}



	extern "C"
	{
		__declspec(dllexport) int NB_IsNpcRemoteSummon(void* pObj)
		{
			oCNpc* pNpc = (oCNpc*)pObj;

			//NB_PrintWin("Mult: " + pNpc->GetInstanceName());

			return (int)remoteSummons.IsInList(pNpc);
		}
	}

	zCArray<oCNpc*> GetVisibleNpcs_NB() {
		zCArray<oCNpc*> npcs;

		player->ClearVobList();
		player->CreateVobList(BROADCAST_DISTANCE);

		zCArray<zCVob*> vobList = player->vobList;

		for (int i = 0; i < vobList.GetNumInList(); i++) {
			if (zCVob* vob = vobList.GetSafe(i)) {

				if (auto pNpc = vob->CastTo<oCNpc>())
				{
					npcs.InsertEnd(pNpc);
				}
			}
		}


		return npcs;
	}


	static int countPrintDebug = 0;
	static zCView* testView = NULL;


	void ClearPrintDebug() {
		countPrintDebug = 0;

		static int init = 0;

		if (!init) {
			init = true;
			testView = new zCView(0, 0, 8196, 8196);
			screen->InsertItem(testView);
		}

		if (testView) {
			testView->SetFontColor(zCOLOR(0, 255, 0));
			testView->ClrPrintwin();
		}

	}

	void PrintDebug(zSTRING str) {

		testView->Print(F(3), F(14) + F(3) * countPrintDebug++, str);
	}

}
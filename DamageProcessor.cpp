namespace GOTHIC_ENGINE {
    void DamageProcessorLoop() {
        PluginState = "DamageProcessorLoop";

        if (ReadyToSyncDamages.isEmpty()) {
            return;
        }

        auto hit = ReadyToSyncDamages.dequeue();

        if (!ClientThread && !ServerThread)
        {
            return;
        }

        if (Myself && hit.attacker && (hit.attacker == Myself->npc || Npc_IsSummon(hit.attacker))) {

            //target uniq name
            hit.npcUniqueName = NpcToUniqueNameList[hit.npc];

            // if target is dead
            if (hit.npc->IsDead()) {
                KilledByPlayerNpcNames[hit.npcUniqueName] = hit.npc;
            }

            // if attack is real player
            if ((hit.attacker == Myself->npc))
            {
                Myself->hitsToSync.push_back(hit);
                //cmd << "HitAtk: " << hit.attacker->GetInstanceName() << ";" << hit.npcUniqueName << endl;
            }
            else
            {
                if (NpcToUniqueNameList.count(hit.attacker))
                {
                    zSTRING atkUniqName = NpcToUniqueNameList[hit.attacker];

                    //cmd << "HitAtkAnother: " << hit.attacker->GetInstanceName() << ";" << hit.npcUniqueName << endl;

                    if (BroadcastNpcs.count(atkUniqName))
                    {
                        BroadcastNpcs[atkUniqName]->hitsToSync.push_back(hit);
                    }
                }
                
            }
           
            
            return;
        }

        if (ServerThread) {
            if (PlayerNpcs.count(hit.attacker)) {
                return;
            }

            auto uniqueNpcName = NpcToUniqueNameList[hit.attacker];
            if (uniqueNpcName && BroadcastNpcs.count(uniqueNpcName)) {
                if (hit.npc == player) {
                    hit.npcUniqueName = "HOST";
                    BroadcastNpcs[uniqueNpcName]->hitsToSync.push_back(hit);
                }
                else if (PlayerNpcs.count(hit.npc)) {
                    hit.npcUniqueName = PlayerNpcs[hit.npc];
                    BroadcastNpcs[uniqueNpcName]->hitsToSync.push_back(hit);
                }
                else if (NpcToUniqueNameList.count(hit.npc)) {
                    hit.npcUniqueName = NpcToUniqueNameList[hit.npc];
                    BroadcastNpcs[uniqueNpcName]->hitsToSync.push_back(hit);
                }
            }
        }
    }
}
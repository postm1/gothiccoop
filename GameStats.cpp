namespace GOTHIC_ENGINE {
    bool displayNetworkStats = false;
    
   

    void GameStatsLoop() {
        PluginState = "GameStatsLoop";

        if (zinput->KeyToggled(ToggleGameStatsKey)) {
            displayNetworkStats = !displayNetworkStats;
            GameChat->Clear();
        }

        GameChat->ClearConst();

        // show connection status on client
        if (ClientThread)
        {
            zSTRING messConnect = isClientConnected ? "ON" : "OFF";
            zCOLOR colorMes = isClientConnected ? GFX_GREEN : GFX_RED;
            GameChat->AddLineConst(messConnect, colorMes, 700, 0);
        }

        if (ServerThread)
        {
            zCOLOR colorMes = lastOnline > 1 ? GFX_GREEN : GFX_RED;
            GameChat->AddLineConst("Players: " + Z lastOnline, colorMes, 700, 0);
        }

        if (zinput->KeyToggled(ToggleGameLogKey)) {
            GameChat->ToggleShowing();
        }

        // calculating network speed
        if (MainTimer[0u].Await(1000))
        {
            speedSent = bytesSent / 1000.0f;
            speedRecieved = bytesRecieved / 1000.0f;

            bytesSent = 0;
            bytesRecieved = 0;
        }

        if (displayNetworkStats) {
            GameChat->Clear();
            ChatLog("readyToSendJsons:");
            ChatLog(ReadyToSendJsons.size());
            ChatLog("readyToBeReceivedPackets:");
            ChatLog(ReadyToBeReceivedPackets.size());
            ChatLog("Income: " + zSTRING(speedRecieved, 2) + " KB/s");
            ChatLog("Outcome: " + zSTRING(speedSent, 2) + " KB/s");

#ifndef NEWBALANCE_MOD
            ChatLog("possition:");
            auto pos = player->GetPositionWorld();
            ChatLog(string::Combine("x: %f y: %f z: %f", pos.n[0], pos.n[1], pos.n[2]));
            if (ServerThread) {
                ChatLog("broadcastNpcs:");
                ChatLog(BroadcastNpcs.size());

                for each (auto i in BroadcastNpcs) {
                    ChatLog(i.first);
                }
            }
            if (ClientThread) {
                ChatLog("SyncNpcs:");
                ChatLog(SyncNpcs.size());

                for each (auto i in SyncNpcs) {
                    ChatLog(i.first);
                }
            }
#endif // NEWBALANCE_MOD
            
        }
    }
}
// Supported with union (c) 2020 Union team
// Union SOURCE file

namespace GOTHIC_ENGINE {
	// Add your code here . . .
    void BuildGlobalNpcList() {
        PluginState = "BuildGlobalNpcList";

        auto* list = ogame->GetGameWorld()->voblist_npcs->next;
        auto firstRun = NpcToFirstRoutineWp.size() == 0;

        if (firstRun) {
            auto* rtnList = rtnMan->rtnList.next;
            while (rtnList) {
                auto rtn = rtnList->data;
                if (NpcToFirstRoutineWp.count(rtn->npc) == 0) {
                    NpcToFirstRoutineWp[rtn->npc] = rtn->wpname.ToChar();
                }
                rtnList = rtnList->next;
            }

            while (list) {
                auto npc = list->data;
                auto objectName = string(npc->GetObjectName());
                auto objectExist = NamesCounter.find(objectName) != NamesCounter.end();
                NamesCounter[objectName] = objectExist ? NamesCounter[objectName] + 1 : 0;
                list = list->next;
            }
        }

        list = ogame->GetGameWorld()->voblist_npcs->next;
        while (list) {
            auto npc = list->data;

            if (npc->IsAPlayer() || npc->GetObjectName().StartWith("FRIEND_") || IgnoredSyncNpc(npc) || NpcToUniqueNameList.count(npc)) {
                list = list->next;
                continue;
            }

            auto name = string(npc->GetObjectName());
            auto objectExist = NamesCounter.find(name) != NamesCounter.end();

            if (objectExist && NamesCounter[name] > 1) {
                auto secondUniquePart = npc->wpname ? string(npc->wpname) : string("UNKNOW");
                if (NpcToFirstRoutineWp.count(npc) > 0 && !NpcToFirstRoutineWp[npc].IsEmpty()) {
                    secondUniquePart = NpcToFirstRoutineWp[npc];
                }
                if (!firstRun) {
                    secondUniquePart = npc->wpname ? string::Combine("DYNAMIC-%s", string(npc->wpname)) : string("DYNAMIC");
                }
                name = string::Combine("%s-%s", name, secondUniquePart);
                objectExist = NamesCounter.find(name) != NamesCounter.end();
                NamesCounter[name] = objectExist ? NamesCounter[name] + 1 : 0;
            }

            auto uniqueName = string::Combine("%s-%i", name, NamesCounter[name]);
            UniqueNameToNpcList[uniqueName] = npc;
            NpcToUniqueNameList[npc] = uniqueName;


            list = list->next;

        }
    }

    void OnClientDisconnected()
    {

        //NB_PrintWin("OnClientDisconnected");

        auto* list = ogame->GetGameWorld()->voblist_npcs->next;

        zCArray<oCNpc*> pRemoveList;


        for (auto syncPlayerNpc : SyncNpcs) {

            if (syncPlayerNpc.second && syncPlayerNpc.second->npc)
            {
                // remove players and theirs summons
                if (Npc_IsRemoteSummon(syncPlayerNpc.second->npc) || syncPlayerNpc.second->npc->GetInstanceName().ToChar() == FriendInstanceId.Upper())
                {
                    if (!pRemoveList.IsInList(syncPlayerNpc.second->npc))
                    {
                        pRemoveList.InsertEnd(syncPlayerNpc.second->npc);
                    }
                }
               
            }
        }
        

        remoteSummons.DeleteList();


        SyncNpcs.clear();
        BroadcastNpcs.clear();
        UniqueNameToNpcList.clear();
        NpcToUniqueNameList.clear();
        NamesCounter.clear();
        NpcToFirstRoutineWp.clear();
        GameChat->Clear();
        LastNpcListRefreshTime = 0;
        LastUpdateListOfVisibleNpcs = 0;
        KilledByPlayerNpcNames.clear();

        Myself = NULL;

        SaveParserVars();

        // actual removing npcs from our own list
        for (int i = 0; i < pRemoveList.GetNumInList(); i++)
        {
            auto pNpc = pRemoveList.GetSafe(i);

            if (pNpc)
            {
                ogame->spawnman->DeleteNpc(pNpc);
            }
        }

        RestoreParserVars();


    }

    void CheckClientDisconnect()
    {
        // check disconnect on client side
        if (isClientConnected && wasClientDisconnect)
        {
            isClientConnected = false;
            wasClientDisconnect = false;


            OnClientDisconnected();
        }
    }
}
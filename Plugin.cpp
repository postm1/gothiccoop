﻿#include "resource.h"

namespace GOTHIC_ENGINE {
    void Game_Entry() {
        GetCurrentDirectory(MAX_PATH, GothicExeFolderPath);

        SymInitialize(GetCurrentProcess(), NULL, true);
        SymSetSearchPath(GetCurrentProcess(), GothicExeFolderPath);

        std::string logFilePath = GothicExeFolderPath;
        logFilePath.append("\\GothicCoopLog.log");
        GothicCoopLogPath = logFilePath;

        std::string configFilePath = GothicExeFolderPath;
        configFilePath.append("\\GothicCoopConfig.json");
        std::ifstream configFile(configFilePath);

        if (configFile.good()) {
            try {
                CoopConfig = json::parse(configFile);
            }
            catch (...) {
                Message::Error("(Gothic Coop) Invalid config file, please check your GothicCoopConfig.json file!");
                exit(1);
            }
        }
        else {
            Message::Error("(Gothic Coop) No config file found!");
            Message::Error(configFilePath.c_str());
            exit(1);
        }

#ifdef NEWBALANCE_MOD
        InitExternalDll();
#endif // NEWBALANCE_MOD

    }

    void Game_Init() {
        MainThreadId = GetCurrentThreadId();
        ToggleGameLogKey = ReadConfigKey("toggleGameLogKey", "KEY_P");
        ToggleGameStatsKey = ReadConfigKey("toggleGameStatsKey", "KEY_O");
        StartServerKey = ReadConfigKey("startServerKey", "KEY_F1");
        StartConnectionKey = ReadConfigKey("startConnectionKey", "KEY_F2");
        ReinitPlayersKey = ReadConfigKey("reinitPlayersKey", "KEY_F3");
        RevivePlayerKey = ReadConfigKey("revivePlayerKey", "KEY_F4");

        //PlayersDamageMultipler = CoopConfig["playersDamageMultipler"].get<int>();
        //NpcsDamageMultipler = CoopConfig["npcsDamageMultipler"].get<int>();
        if (CoopConfig.contains("friendInstanceId")) {
            FriendInstanceId = string(CoopConfig["friendInstanceId"].get<std::string>().c_str()).ToChar();
        }
        if (CoopConfig.contains("nickname")) {
            MyNickname = string(CoopConfig["nickname"].get<std::string>().c_str()).ToChar();
        }


        if (CoopConfig.contains("interpolateValue")) {
            interpolateValue = CoopConfig["interpolateValue"].get<float>();
        }
        
        if (CoopConfig.contains("portSocket")) {
            portSocket = CoopConfig["portSocket"].get<int>();
        }

#ifdef NEWBALANCE_MOD
        NB_SetMultiplayerOn();
#endif // NEWBALANCE_MOD
    }

   

    void Game_Loop() {
        PluginState = "GameLoop";
        if (IsLoadingLevel) {
            return;
        }

        CurrentMs = GetCurrentMs();

        // Ticks the timer
        MainTimer.ClearUnused();

        ClearPrintDebug();
        //npcMan.Loop(); //test code

        GameChat->Render();
        GameStatsLoop();
        PacketProcessorLoop();
        DamageProcessorLoop();
        SpellCastProcessorLoop();
        ReviveFriendLoop();

        if (CurrentMs > LastNpcListRefreshTime + 1000) {
            BuildGlobalNpcList();
            LastNpcListRefreshTime = CurrentMs;
            PluginState = "GameLoop";
        }

        if (CurrentMs > LastUpdateListOfVisibleNpcs + 500) {
            UpdateVisibleNpc();
            LastUpdateListOfVisibleNpcs = CurrentMs;
            PluginState = "GameLoop";
        }

        if ((ClientThread || ServerThread) && !Myself) {
            Myself = new LocalNpc(player, MyselfId);
        }


        CheckClientDisconnect();

        if (player && player->GetInstanceName() != lastPlayerInst)
        {
            if (Myself)
            {
                cmd << "Transform into: " << player->GetInstanceName() << endl;

                lastPlayerInst = player->GetInstanceName();
                Myself->npc = player;
                Myself->SyncGuildChange();

                // it player return to human, update armor and etc
                if (player->GetInstanceName() == "PC_HERO")
                {
                    Myself->Reinit();
                }
            }
        }

        

        if (!IsCoopPaused) {

            /*
            if (ServerThread)
            {
                if (MainTimer[2u].Await(3000))
                {
                    NB_PrintWin("REINIT");

                    if (Myself)
                    {
                        Myself->Reinit();

                    }

                    for (auto const& pair : SyncNpcs) {
                        auto npc = pair.second;

                        if (npc->npc && npc->npc->GetInstanceName() == FriendInstanceId.Upper().ToChar())
                        {
                            npc->InitCoopFriendNpc();
                        }
                    }

                }
            }
            */

            PluginState = "PulseMyself";
            if (Myself) {
                Myself->Pulse();
                Myself->PackUpdate();
            }

            PluginState = "PulseBroadcastNpcs";
            for (auto p : BroadcastNpcs) {

                /*
                if (ClientThread)
                {
                    if (p.second && p.second->npc)
                    {
                        cmd << p.second->npc->GetInstanceName() << " " << p.second->name << endl;
                    }
                }
                */
               p.second->Pulse();
               p.second->PackUpdate();
             
            }

            PluginState = "UpdateSyncNpcs";
            for (auto const& pair : SyncNpcs) {
                auto npc = pair.second;
                npc->Update();

                if (npc->destroyed) {
                    SyncNpcs.erase(pair.first);
                }
            }
        }

        if (!IsPlayerTalkingWithAnybody()) {
            if (zinput->KeyToggled(StartServerKey) && !ServerThread && !ClientThread) {
                
                
                wchar_t mappedPort[1234];
                string s = string(portSocket);

                std::wcsncpy(mappedPort, L"UDP", s.Length());
                new MappedPort(portSocket, mappedPort, mappedPort);
                

                Thread t;
                t.Init(&CoopServerThread);
                t.Detach();
                ServerThread = &t;
                MyselfId = "HOST";
            }

            if (zinput->KeyToggled(StartConnectionKey) && !ServerThread)
            {
                if (!isClientConnected)
                {
                    // reconnect
                    if (ClientThread)
                    {
                        ClientThread->Break();
                        ClientThread = NULL;

                     

                    }

                    if (!ClientThread) {
                        addSyncedNpc("HOST");

                        Thread  t;
                        t.Init(&CoopClientThread);
                        t.Detach();
                        ClientThread = &t;
                    }
                    ogame->SetTime(ogame->GetWorldTimer()->GetDay(), 12, 00);
                    rtnMan->RestartRoutines();
                }
                else {
                    if (IsCoopPaused) {
                        ChatLog("Restoring world synchronization.");
                    }
                    else {
                        ChatLog("Stop world synchronization.");
                    }

                    IsCoopPaused = !IsCoopPaused;
                    rtnMan->RestartRoutines();
                }
            }



            if (zinput->KeyToggled(ReinitPlayersKey)) {
                for (auto playerNpcToName : PlayerNpcs) {
                    auto name = playerNpcToName.second;
                    if (SyncNpcs.count(name)) {
                        NB_PrintWin("Reset players");
                        SyncNpcs[name]->InitCoopFriendNpc();
                    }
                }
            }
        }
    }

    void Game_SaveBegin() {
        IsSavingGame = true;
    }

    void Game_SaveEnd() {
        for (auto playerNpcToName : PlayerNpcs) {
            auto name = playerNpcToName.second;
            if (SyncNpcs.count(name)) {
                SyncNpcs[name]->InitCoopFriendNpc();
            }
        }

        IsSavingGame = false;
    }

    void LoadBegin() {
        IsLoadingLevel = true;
        Myself = NULL;
        //npcMan.Clear();
    }

    void LoadEnd() {
        if (ServerThread || ClientThread) {
            Myself = new LocalNpc(player, MyselfId);
        }

        //npcMan.Clear();

        remoteSummons.DeleteList();

        std::map<string, RemoteNpc*> syncPlayerNpcs;
        for (auto playerNpcToName : PlayerNpcs) {
            auto name = playerNpcToName.second;
            if (SyncNpcs.count(name)) {
                syncPlayerNpcs[name] = SyncNpcs[name];
            }
        }

        SyncNpcs.clear();

        // wtf?
        for (auto syncPlayerNpc : syncPlayerNpcs) {
            SyncNpcs[syncPlayerNpc.first] = syncPlayerNpc.second;
        }

        BroadcastNpcs.clear();
        UniqueNameToNpcList.clear();
        NpcToUniqueNameList.clear();
        NamesCounter.clear();
        NpcToFirstRoutineWp.clear();
        GameChat->Clear();
        LastNpcListRefreshTime = 0;
        LastUpdateListOfVisibleNpcs = 0;
        KilledByPlayerNpcNames.clear();

        auto totWp = ogame->GetWorld()->wayNet->GetWaypoint("TOT");
        if (totWp) {
            CurrentWorldTOTPosition = &totWp->GetPositionWorld();
        }
        else {
            CurrentWorldTOTPosition = NULL;
        }

        IsLoadingLevel = false;
    }

    void Game_LoadEnd_SaveGame() {
        LoadEnd();

        auto coopFriendInstanceId = GetFriendDefaultInstanceId();
        auto* list = ogame->GetGameWorld()->voblist_npcs->next;

        zCArray<oCNpc*> pRemoveList;

        while (list) {
            auto npc = list->data;
            if (npc && npc->GetInstance() == coopFriendInstanceId && npc->GetAttribute(NPC_ATR_STRENGTH) == COOP_MAGIC_NUMBER) {
                
                // add npc for a separate removing list, because we can't remove npc from global list directly in a cycle, that will cause some bugs
                pRemoveList.InsertEnd(npc);
                
            }
            list = list->next;
        }

        SaveParserVars();

        // actual removing npcs from our own list
        for (int i = 0; i < pRemoveList.GetNumInList(); i++)
        {
            auto pNpc = pRemoveList.GetSafe(i);

            if (pNpc)
            {
                ogame->spawnman->DeleteNpc(pNpc);
            }
        }

        RestoreParserVars();
    }

    void Game_LoadBegin_Trigger() {
        return;
    }
}

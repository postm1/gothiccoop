namespace GOTHIC_ENGINE {

    void CoopLog(std::string l)
    {
        std::ofstream CoopLog(GothicCoopLogPath, std::ios_base::app | std::ios_base::out);
        CoopLog << l;
    }

    void ChatLog(string text, zCOLOR color = zCOLOR(255, 255, 255, 255)) {
        GameChat->AddLine(text, color);

#ifdef NEWBALANCE_MOD
        //NB_PrintWin(text.ToChar());
#endif // NEWBALANCE_MOD

        
    };

    int GetFreePlayerId() {
        LastFreePlayerId += 1;
        return LastFreePlayerId;
    }

    bool IsCoopPlayer(std::string name) {
        string cStringName = name.c_str();
        return cStringName == "HOST" || cStringName.StartWith("FRIEND_");
    }

    bool IsCoopPlayer(string name) {
        return name == "HOST" || name.StartWith("FRIEND_");
    }

    oCItem* CreateCoopItem(int insIndex) {
        return zfactory->CreateItem(insIndex);
    }

    oCItem* CreateCoopItem_NB(oCNpc* pNpc, int index) {

        oCItem* item = pNpc->IsInInv(index, 1);

        if (!item)
        {
            pNpc->CreateItems(index, 1);
            item = pNpc->IsInInv(index, 1);
        }

        return item;
    }

    std::vector<oCNpc*> GetVisibleNpcs() {
        std::vector<oCNpc*> npcs;

        player->ClearVobList();
        player->CreateVobList(BROADCAST_DISTANCE);

        zCArray<zCVob*> vobList = player->vobList;

        for (int i = 0; i < vobList.GetNumInList(); i++) {
            if (zCVob* vob = vobList.GetSafe(i)) {

                if (auto pNpc = vob->CastTo<oCNpc>())
                {
                    if (!pNpc->IsAPlayer() && !pNpc->GetObjectName().StartWith("FRIEND_")) {
                        npcs.push_back(pNpc);
                    }
                }
            }
        }


        return npcs;
    }

    float GetHeading(oCNpc* npc)
    {
        float x = *(float*)((DWORD)npc + 0x44);
        float rotx = asin(x) * 180.0f / 3.14f;
        float y = *(float*)((DWORD)npc + 0x64);
        if (y > 0)
        {
            if (x < 0)
                rotx = 360 + rotx;
        }
        else
        {
            if (rotx > 0)
                rotx = 180 - rotx;
            else
            {
                rotx = 180 + rotx;
                rotx = 360 - rotx;
            }
        }
        return rotx;
    };

    int ReadConfigKey(std::string key, string _default) {
        auto stringKey = CoopConfig.contains(key) ?
            string(CoopConfig[key].get<std::string>().c_str()).ToChar() :
            _default;

        return GetEmulationKeyCode(stringKey);
    }

    bool IsPlayerTalkingWithAnybody() {
        return player->talkOther || !oCInformationManager::GetInformationManager().HasFinished() || (ogame->GetCameraAI() && ogame->GetCameraAI()->GetMode().Compare("CAMMODDIALOG"));
    }

    bool IsPlayerTalkingWithNpc(zCVob* npc) {
        if (player->talkOther == npc) {
            return true;
        }

        if (ogame->GetCameraAI() && ogame->GetCameraAI()->GetMode().Compare("CAMMODDIALOG") && ogame->GetCameraAI()->targetVobList.GetNum() > 0) {
            for (int i = 0; i < ogame->GetCameraAI()->targetVobList.GetNum(); i++) {
                auto vob = ogame->GetCameraAI()->targetVobList[i];
                if (npc == vob) {
                    return true;
                }
            }
        }

        return false;
    }

    bool IgnoredSyncNpc(zCVob* npc) {
        auto name = npc->GetObjectName();

        for (unsigned int i = 0; i < IgnoredSyncNpcsCount; i++)
        {
            if (strcmp(name.ToChar(), IgnoredSyncNpcs[i]) == 0)
                return true;
        }

        return false;
    }

    // NB only
    bool Npc_IsSummon(oCNpc* pNpc)
    {
        return (pNpc && pNpc->aiscriptvars[91] == 13771 && pNpc->GetInstanceName() != "NIXDOG" && pNpc->GetInstanceName() != "DK_PETSHOW");
    }

    // check if summon npc is remote, not local
    bool Npc_IsRemoteSummon(oCNpc* pNpc)
    {
        return (Npc_IsSummon(pNpc) && remoteSummons.IsInList(pNpc));
    }

    

    long long GetCurrentMs() {
        std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()
            );

        return ms.count();
    }

    static int GetPartyMemberID() {
        zCPar_Symbol* sym = parser->GetSymbol("AIV_PARTYMEMBER");
        if (!sym)
#if ENGINE >= Engine_G2
            return 15;
#else
            return 36;
#endif
        int id;
        sym->GetValue(id, 0);
        return id;
    }

    static int GetFriendDefaultInstanceId() {
        auto instId = parser->GetIndex(FriendInstanceId);

        return instId;
    }
}